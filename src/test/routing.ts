import * as assert from 'assert'
import { HttpMessageBuilder, HttpMethod, HttpMessage } from '../httpmessage'
import { Router, RouteOptions, RouteCallback } from '../routing'

describe('router', () => {

    describe('matches', () => {

        async function testRoute(routeOptions: RouteOptions, callback: RouteCallback,
                request: HttpMessage, body: string, code: number = 200) {
            // Setup router
            const router = new Router()
            const opts = router.set(routeOptions, callback)

            // Attempt match
            const res1 = await router.match(request)
            assert.equal(res1.statusCode, code)
            assert.equal(res1.body, body)

            // Remote route and attempt match again
            router.remove(opts)
            const res2 = await router.match(request)
            assert.equal(res2.statusCode, 404)
        }

        ['DELETE', 'GET', 'PATCH', 'POST', 'PUT'].forEach((method: HttpMethod) => {

            it(`${method} exact path`, async () => {
                const rnd = String(Date.now())
                const opts: RouteOptions = {path: `/${rnd}`, method: method}
                const callback: RouteCallback = () => rnd
                const request = new HttpMessageBuilder()
                    .setMethod(method)
                    .setPath(`/${rnd}`)
                await testRoute(opts, callback, request, rnd)
            })

            it(`${method} root path`, async () => {
                const rnd = String(Date.now())
                const opts: RouteOptions = {path: `/`, method: method}
                const callback: RouteCallback = () => rnd
                const request = new HttpMessageBuilder()
                    .setMethod(method)
                    .setPath(`/`)
                await testRoute(opts, callback, request, rnd)
            })

            it(`${method} simple path with one parameter`, async () => {
                const paramName = 'param1'
                const rnd = String(Date.now())
                const opts: RouteOptions = {path: `/:${paramName}`, method: method}
                const callback: RouteCallback = (_, params) => params[paramName]
                const request = new HttpMessageBuilder()
                    .setMethod(method)
                    .setPath(`/${rnd}`)
                await testRoute(opts, callback, request, rnd)
            })

            it(`${method} leading path with one parameter`, async () => {
                const paramName = 'param1'
                const rnd = String(Date.now())
                const opts: RouteOptions = {path: `/a/b/c/:${paramName}`, method: method}
                const callback: RouteCallback = (_, params) => params[paramName]
                const request = new HttpMessageBuilder()
                    .setMethod(method)
                    .setPath(`/a/b/c/${rnd}`)
                await testRoute(opts, callback, request, rnd)
            })

            it(`${method} trailing path with one parameter`, async () => {
                const paramName = 'param1'
                const rnd = String(Date.now())
                const opts: RouteOptions = {path: `/:${paramName}/a/b/c`, method: method}
                const callback: RouteCallback = (_, params) => params[paramName]
                const request = new HttpMessageBuilder()
                    .setMethod(method)
                    .setPath(`/${rnd}/a/b/c`)
                await testRoute(opts, callback, request, rnd)
            })

            it(`${method} trailing optional path`, async () => {
                const paramName = 'param1'
                const rnd = String(Date.now())
                const opts: RouteOptions = {path: `/a/b?`, method: method}
                const callback: RouteCallback = () => rnd
                const request1 = new HttpMessageBuilder()
                    .setMethod(method)
                    .setPath(`/a`)
                const request2 = new HttpMessageBuilder()
                    .setMethod(method)
                    .setPath(`/a/b`)
                await testRoute(opts, callback, request1, rnd)
                await testRoute(opts, callback, request2, rnd)
            })

            it(`${method} trailing optional path with one parameter`, async () => {
                const paramName = 'param1'
                const rnd = String(Date.now())
                const opts: RouteOptions = {path: `/:${paramName}/a?`, method: method}
                const callback: RouteCallback = (_, params) => params[paramName]
                const request1 = new HttpMessageBuilder()
                    .setMethod(method)
                    .setPath(`/${rnd}`)
                const request2 = new HttpMessageBuilder()
                    .setMethod(method)
                    .setPath(`/${rnd}/a`)
                await testRoute(opts, callback, request1, rnd)
                await testRoute(opts, callback, request2, rnd)
            })

            it(`${method} trailing optional parameter`, async () => {
                const paramName = 'param1'
                const rnd = String(Date.now())
                const opts: RouteOptions = {path: `/a/:${paramName}?`, method: method}
                const callback: RouteCallback = (_, params) => params[paramName]
                const request1 = new HttpMessageBuilder()
                    .setMethod(method)
                    .setPath(`/a`)
                const request2 = new HttpMessageBuilder()
                    .setMethod(method)
                    .setPath(`/a/${rnd}`)
                await testRoute(opts, callback, request1, '')
                await testRoute(opts, callback, request2, rnd)
            })

            it(`${method} parameter followed by optional parameter`, async () => {
                const paramName1 = 'param1'
                const paramName2 = 'param2'
                const rnd1 = String(Date.now())
                const rnd2 = String(Math.random()).substr(2)
                const opts: RouteOptions = {
                    path: `/:${paramName1}/:${paramName2}?`, method: method}
                const callback: RouteCallback = (_, params) =>
                    `${params[paramName1] || ''}:${params[paramName2] || ''}`
                const request1 = new HttpMessageBuilder()
                    .setMethod(method)
                    .setPath(`/${rnd1}`)
                const request2 = new HttpMessageBuilder()
                    .setMethod(method)
                    .setPath(`/${rnd1}/${rnd2}`)
                await testRoute(opts, callback, request1, `${rnd1}:`)
                await testRoute(opts, callback, request2, `${rnd1}:${rnd2}`)
            })

            it(`${method} path without starting slash`, async () => {
                const rnd = String(Date.now())
                const opts: RouteOptions = {path: `${rnd}`, method: method}
                const callback: RouteCallback = () => rnd
                const request = new HttpMessageBuilder()
                    .setMethod(method)
                    .setPath(`/${rnd}`)
                await testRoute(opts, callback, request, rnd)
            })

            it(`${method} path with trailing slash`, async () => {
                const rnd = String(Date.now())
                const opts1: RouteOptions = {path: `/${rnd}`, method: method}
                const opts2: RouteOptions = {path: `/${rnd}/`, method: method}
                const callback: RouteCallback = () => rnd
                const request1 = new HttpMessageBuilder()
                    .setMethod(method)
                    .setPath(`/${rnd}`)
                const request2 = new HttpMessageBuilder()
                    .setMethod(method)
                    .setPath(`/${rnd}/`)

                await testRoute(opts1, callback, request1, rnd)
                await testRoute(opts1, callback, request2, '', 404)

                await testRoute(opts2, callback, request2, rnd)
                await testRoute(opts2, callback, request1, '', 404)
            })

            it(`${method} path with two slashes`, async () => {
                const rnd = String(Date.now())
                const opts: RouteOptions = {path: `/a//b`, method: method}
                const callback: RouteCallback = () => rnd
                const request = new HttpMessageBuilder()
                    .setMethod(method)
                    .setPath(`/a//b`)
                await testRoute(opts, callback, request, rnd)
            })

            it(`${method} path with empty trailing parameter`, async () => {
                const paramName = 'param1'
                const opts: RouteOptions = {path: `/a/:${paramName}`, method: method}
                const callback: RouteCallback = (_, params) => params[paramName]
                const request1 = new HttpMessageBuilder()
                    .setMethod(method)
                    .setPath(`/a`)
                const request2 = new HttpMessageBuilder()
                    .setMethod(method)
                    .setPath(`/a/`)
                await testRoute(opts, callback, request1, '', 404)
                await testRoute(opts, callback, request2, '', 200)
            })

            it(`${method} path with empty leading parameter`, async () => {
                const paramName = 'param1'
                const opts: RouteOptions = {path: `/:${paramName}/a`, method: method}
                const callback: RouteCallback = (_, params) => params[paramName]
                const request = new HttpMessageBuilder()
                    .setMethod(method)
                    .setPath(`//a`)
                await testRoute(opts, callback, request, '')
            })

            it(`${method} path with empty middle parameter`, async () => {
                const paramName = 'param1'
                const opts: RouteOptions = {path: `/a/:${paramName}/b`, method: method}
                const callback: RouteCallback = (_, params) => params[paramName]
                const request = new HttpMessageBuilder()
                    .setMethod(method)
                    .setPath(`/a//b`)
                await testRoute(opts, callback, request, '')
            })

            it(`${method} async callback`, async () => {
                const rnd = String(Date.now())
                const opts: RouteOptions = {path: `/${rnd}`, method: method}
                const callback: RouteCallback = async () => rnd
                const request = new HttpMessageBuilder()
                    .setMethod(method)
                    .setPath(`/${rnd}`)
                await testRoute(opts, callback, request, rnd)
            })

            it(`${method} async callback with one parameter`, async () => {
                const paramName = 'param1'
                const rnd = String(Date.now())
                const opts: RouteOptions = {path: `/:${paramName}`, method: method}
                const callback: RouteCallback = async (_, params) => params[paramName]
                const request = new HttpMessageBuilder()
                    .setMethod(method)
                    .setPath(`/${rnd}`)
                await testRoute(opts, callback, request, rnd)
            })

            it(`${method} with query string`, async () => {
                const rnd1 = String(Date.now())
                const rnd2 = String(Math.random()).substr(2)
                const opts: RouteOptions = {path: `/${rnd1}`, method: method}
                const callback: RouteCallback = () => rnd2
                const request = new HttpMessageBuilder()
                    .setMethod(method)
                    .setPath(`/${rnd1}?ts=${rnd2}`)
                await testRoute(opts, callback, request, rnd2)
            })

            it(`${method} parameter and query string`, async () => {
                const paramName = 'param1'
                const rnd1 = String(Date.now())
                const rnd2 = String(Math.random()).substr(2)
                const opts: RouteOptions = {path: `/:${paramName}`, method: method}
                const callback: RouteCallback = (_, params) => params[paramName]
                const request = new HttpMessageBuilder()
                    .setMethod(method)
                    .setPath(`/${rnd1}?q=${rnd2}`)
                await testRoute(opts, callback, request, rnd1)
            })

            it(`${method} throw error`, async () => {
                const rnd = String(Date.now())
                const opts: RouteOptions = {path: `/${rnd}`, method: method}
                const error = new Error('Intentional error')
                const callback: RouteCallback = () => { throw(error) }
                const request = new HttpMessageBuilder()
                    .setMethod(method)
                    .setPath(`/${rnd}`)
                await testRoute(opts, callback, request, 'Server error: ' + error.message, 500)
            })
        })
    })
})
