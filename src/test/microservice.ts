import * as assert from 'assert'
// import * as request from 'supertest'
import { HttpRequest, WebSocketRequest } from '../request'
import { MicroService, HttpMessageBuilder } from '../microservice'

const json = {"headlines":[{"description":"Authorities say it's unclear what started the fire at the popular tourist attraction."},{"description":"The bike, made by BigRep, took three days to print and cost about Â£2,000 ($2,600) to produce."},{"description":"No women featured in the announcement celebrating the Gulf state's efforts to reduce the gender gap."},{"description":"Trevor, the only duck on the Pacific island of Niue, lived in a puddle and was cared for by locals."},{"description":"An etiquette course in an Istanbul municipality advises women not to lick ice cream."},{"description":"An Italian policeman runs a race in Siberia braving temperatures of -52 Celsius."},{"description":"Climate change could make South Korea a banana producer."},{"description":"A look back at photojournalist Peter Dench's journey on the Trans-Siberian Railway during the World Cup in Russia 2018."},{"description":"A controversial new abortion law has been enacted in New York state, but why has it caused such a heated debate?"}]}

describe('microservice', () => {
    let service: MicroService
    const requestTypes = ['HttpRequest', 'WebSocketRequest']

    before(async () => {
        service = await MicroService.create()
        service.route('/', async _ =>
            new HttpMessageBuilder().setBody('Hello World').build())
        service.route('/object', async _ =>
            new HttpMessageBuilder().setBody({0: Math.random()}).build())
        service.route('/random', async _ =>
            new HttpMessageBuilder().setBody('' + Math.random()).build())
        service.route({path: '/params', method: 'GET', mandatoryQueryParameters: ['test']},
            async _ => new HttpMessageBuilder().setBody('' + Math.random()).build())
        service.route('/object_raw', async () =>
            new HttpMessageBuilder().setBody(json).build())
    })


    beforeEach(async () => {
        service.stop()
        await service.clearCache()
        service.start(Math.floor(Math.random() * (9999 - 1024)) + 1024)
    })

    afterEach(() => {
        service.stop()
    })

    requestTypes.forEach(name => {
        let request: () => (HttpRequest | WebSocketRequest)
        if (name === 'HttpRequest')
            request = () => new HttpRequest(service.server)
        if (name === 'WebSocketRequest')
            request = () => new WebSocketRequest(service.server)


        describe(`${name} cache`, () => {

            it('records incoming request', async () => {
                const path = '/?q=' + Math.random().toString()
                const res = await request().get(path)
                const cache = await service.db.get(`
                    SELECT * FROM cache
                    ORDER BY timestamp DESC LIMIT 1`)
                assert.equal(cache.url, path)
            })

            it('repeats consecutive requests', async () => {
                const path = '/random'
                const res1 = await request().get(path)
                const res2 = await request().get(path)
                assert.equal(res1.body.toString(), res2.body.toString())
            })

            it('expires', async () => {
                const path = '/random'
                const seconds = service.cacheExpirySeconds
                service.cacheExpirySeconds = 0
                const res1 = await request().get(path)
                await new Promise<void>((resolve, _) => setTimeout(resolve, 1000))
                const res2 = await request().get(path)
                assert.notEqual(res1.body, res2.body)
                service.cacheExpirySeconds = seconds
            })

            it('converts non-string output', async () => {
                const path = '/object'
                const res1 = await request().get(path)
                const cache = await service.db.get(`
                    SELECT * FROM cache
                    ORDER BY timestamp DESC LIMIT 1`)
                const res2 = await request().get(path)
                // console.log(res1, res2, cache)
                assert.deepEqual(res1.json, res2.json, 'Compare body JSON')
                assert.deepEqual(res1.body.toJSON(), res2.body.toJSON(), 'Compare body Buffers')
                assert.deepEqual(JSON.parse(cache.body_out), res1.body.toJSON(), 'Compare with cache')
            })

            it('handles utf-8 text', async () => {
                // Setup routes for this test
                const rnd = String(Math.random()).substr(2)
                const path = `/${rnd}`
                const route = service.route({path: path, method: 'GET'}, async () =>
                    new HttpMessageBuilder().setBody('的').build())

                const res1 = await request().get(path)
                const cache = await service.db.get(`
                    SELECT * FROM cache
                    ORDER BY timestamp DESC LIMIT 1`)
                const res2 = await request().get(path)
                assert.equal(res1.body.toString('utf-8'), res2.body.toString('utf-8'))
                assert.deepEqual(Buffer.from(JSON.parse(cache.body_out)).toJSON(), res1.body.toJSON())

                // Clear routes created by this test
                service.unroute(route)
            })

            it('does not cache different headers', async () => {
                const path = '/random'
                const res1 = await request().set('test', 'foobar').get(path)
                const res2 = await request().set('test', 'barfoo').get(path)
                assert.notEqual(res1.body, res2.body)
            })

            it('replays outbound headers', async () => {
                // Setup routes for this test
                const path = '/header'
                const route = service.route({path: path, method: 'GET'}, async request =>
                    new HttpMessageBuilder()
                        .setHeader('test', request.path.split('test=')[1]).build())

                const val = String(Date.now())
                const query = 'test=' + val
                const res1 = await request().get(`${path}?${query}`)
                const cache = await service.db.get(`
                    SELECT * FROM cache
                    ORDER BY timestamp DESC LIMIT 1`)
                const res2 = await request().get(`${path}?${query}`)
                const stored = JSON.parse(cache.headers_out)
                assert.equal(res1.headers.test, val)
                assert.equal(res1.headers.test, stored.test)
                assert.equal(res1.headers.test, res2.headers.test)

                // Clear routes created by this test
                service.unroute(route)
            })
        })

        describe(`${name} route`, () => {

            it('route() and unroute()', async () => {
                const path = '/testroute'
                const opts = service.route(path, _ => 'OK')
                const res1 = await request().get(path)
                assert.equal(res1.statusCode, 200)

                const flag = service.unroute(opts)
                assert.equal(flag, true)

                const res2 = await request().get(path)
                assert.equal(res2.statusCode, 404)
            })

            it('route() return types', async () => {
                let idx = 0
                const path_ = () => `/messagetype${++idx}`

                // Proper return type
                let path = path_()
                let opts = service.route(path, _ =>
                    new HttpMessageBuilder().setBody('OK').build())
                let res = await request().get(path)
                assert.equal(res.statusCode, 200)
                service.unroute(opts)

                // Plain string
                path = path_()
                opts = service.route(path, _ => 'OK')
                res = await request().get(path)
                assert.equal(res.statusCode, 200)
                assert.equal(res.body, 'OK')
                service.unroute(opts)

                // Empty string
                path = path_()
                opts = service.route(path, _ => '')
                res = await request().get(path)
                assert.equal(res.statusCode, 200)
                assert.equal(res.body, '')
                service.unroute(opts)

                // Object type
                path = path_()
                const obj = {data: 'OK'}
                opts = service.route(path, _ => obj)
                res = await request().get(path)
                assert.equal(res.statusCode, 200)
                assert.equal(res.body, JSON.stringify(obj))
                assert.deepEqual(res.json, obj)
                service.unroute(opts)
            })

            it('with truthy mandatory parameters', async () => {
                const res1 = await request().get('/params')
                const res2 = await request().get('/params?test=null')
                assert.equal(400, res1.statusCode)
                assert.equal(200, res2.statusCode)
            })

            it('with falsy mandatory parameters', async () => {
                const res1 = await request().get('/params')
                const res2 = await request().get('/params?test=')
                assert.equal(400, res1.statusCode)
                assert.equal(200, res2.statusCode)
            })

            it('callback `conn` parameter', async () => {
                const path = `/${Date.now()}`
                const checkConn = (conn: any) => name === 'WebSocketRequest' ? !!conn : !conn;
                const opts = service.route(path, (request, params, conn) =>
                    new HttpMessageBuilder().setStatusCode(checkConn(conn) ? 200 : 400).build())
                const res = await request().get(path)
                assert.equal(res.statusCode, 200)
                service.unroute(opts)
            })

            it('POST request', async () => {
                const path = '/post'
                const opts = service.route({path: path, method: 'POST'},
                    async request => await request.body || '')
                const res1 = await request().get(path)
                const res2 = await request().post(path)
                const res3 = await request().post(path, '{}')
                const res4 = await request().post(path, 'OK')
                assert.equal(res1.statusCode, 404)
                assert.equal(res2.statusCode, 200)
                assert.equal(res3.statusCode, 200)
                assert.equal(res4.statusCode, 200)
                assert.equal(res2.body, '')
                assert.equal(res3.body, '{}')
                assert.equal(res4.body, 'OK')
                service.unroute(opts)
            })
            // TODO: test DELETE, POST, PUT routes
        })

        describe(`${name} benchmark`, () => {

            // Disable logging during benchmark
            let tmplog: (level: string, ...args: any[]) => Promise<string> = null
            beforeEach(async () => {
                tmplog = service.log
                service.log = async (level: string, ...args: any[]) => null
            })
            afterEach(() => {
                service.log = tmplog
            })

            // Define a helper function to time repetitions
            interface TimeLoopOptions {
                targetPerSecond?: number,
                iterationCount?: number,
                warmupCount?: number,
                batchSize?: number
            }
            const _timeLoopDefaults = {
                targetPerSecond: 50,
                iterationCount: 1000,
                warmupCount: 250,
                batchSize: 8} as TimeLoopOptions
            async function timeLoop(func: () => Promise<any>, opts: TimeLoopOptions = {}) {
                opts = Object.assign({}, opts, _timeLoopDefaults)

                // Do not time the first `skip` iterations of the loop
                for (let i = 0; i < opts.warmupCount; i++) {
                    await func()
                }

                // Start the clock and run the remaining iterations
                const start = Date.now()
                const awaitPool: Promise<any>[] = []
                for (let i = 0; i < opts.iterationCount; i++) {
                    awaitPool.push(func())
                    if (awaitPool.length >= opts.batchSize) {
                        await Promise.all(awaitPool)
                        awaitPool.splice(0, awaitPool.length)
                    }
                }
                await Promise.all(awaitPool)

                // Compute the iterations per seconds and assert that we meet target
                const totaltime = Date.now() - start
                const persecond = (opts.iterationCount - opts.warmupCount) / (totaltime / 1000.0)
                const msg = `Requests per second: ${persecond.toFixed(2)}. Total time: ${totaltime} ms`
                console.log(msg)
                assert(persecond > opts.targetPerSecond, msg)
            }

            it('random route', async () => {
                const req = request()
                await timeLoop(() => req.get(`/${Date.now()}`))
            }).timeout(20000)

            it('database read', async () => {
                const req = request()
                const path = '/dbreadtest'
                const opts = service.route({path: path, method: 'POST'},
                    async _ => await service.db.get(`SELECT * FROM logs`))
                await timeLoop(() => req.get(`/dbreadtest?ts=${Date.now()}`))
                service.unroute(opts)
            }).timeout(20000)

            it('large body', async () => {
                // Create large text
                let text = ''
                for (let i = 0; i < 1024; i++)
                    text += Math.random().toString(36).substr(2)
                // for (let i = 0; i < 16; i++)
                //     text += text

                const req = request()
                const path = '/largebodytest'
                const opts = service.route({path: path, method: 'POST'}, async _ => text)

                // Start test loop and destroy routes after done
                await timeLoop(async () => req.get(`/largebodytest?ts=${Date.now()}`))
                service.unroute(opts)
            }).timeout(20000)

            it('cached', async () => {
                const req = request()
                const path = '/cached'
                const opts = service.route({path: path, method: 'POST'}, async _ => 'OK')
                await timeLoop(() => req.get(path))
                service.unroute(opts)
            }).timeout(20000)

            it('request flooding', async () => {
                const req = request()
                await timeLoop(() =>
                    req.get(`/${Date.now()}`), {batchSize: 1000})
            }).timeout(20000)

        })
    })
})
