import * as assert from 'assert'
// import * as request from 'supertest'
import { HttpRequest, WebSocketRequest } from '../request'
import { MicroService, HttpMessageBuilder } from '../microservice'

describe('manager', () => {
    let service: MicroService

    before(async () => {
        service = await MicroService.create()
        service.route('/', async _ =>
            new HttpMessageBuilder().setBody('Hello World').build())
        service.route('/object', async _ =>
            new HttpMessageBuilder().setBody({0: Math.random()}).build())
        service.route('/random', async _ =>
            new HttpMessageBuilder().setBody('' + Math.random()).build())
        service.route({path: '/params', method: 'GET', mandatoryQueryParameters: ['test']},
            async _ => new HttpMessageBuilder().setBody('' + Math.random()).build())
    })

    beforeEach(async () => {
        service.stop()
        await service.clearCache()
        service.start(Math.floor(Math.random() * (9999 - 1024)) + 1024)
    })

    afterEach(() => {
        service.stop()
    })

    describe(`Connection`, () => {

        it('Microservice talks back', async () => {
            const service = await MicroService.create()
            service.start(Math.floor(Math.random() * (9999 - 1024)) + 1024)

            const path = '/testroute'
            const opts = service.route(path, (request, params, conn) => {
                conn.send(new HttpMessageBuilder().setBody('1').build())
                conn.send(new HttpMessageBuilder().setBody('2').build())
                return new HttpMessageBuilder().setBody('OK').build()
            })

            const responses: string[] = []
            const request = new WebSocketRequest(service.server)
            const manager = await request.manager
            manager.on('message', message => responses.push(message.body.toString('utf-8')))
            const res = await request.get(path)
            assert.equal(res.statusCode, 200)
            assert.equal(res.body.toString('utf-8'), 'OK')
            assert.deepEqual(responses, ['1', '2', 'OK'])

            service.stop()
        })
    })
})
