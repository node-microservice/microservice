import { OutgoingHttpHeaders, IncomingMessage } from 'http'
import { AddressInfo } from 'net'
import { StreamAsync } from 'stream-buffer-async'
import { Cooky, SetCookie } from './cooky'

export type HttpMethod = 'DELETE' | 'GET' | 'PATCH' | 'POST' | 'PUT'
export type ParseableHttpMessage = HttpMessage | string | object
export type AwaitableHttpMessage = ParseableHttpMessage | Promise<ParseableHttpMessage>

function isHttpMessage(message: Object): message is HttpMessage {
    const props = ['protocol', 'path', 'method', 'statusCode', 'contentType', 'headers', 'cookies',
        'body', 'json']
    return props.every(key => message.hasOwnProperty(key))
}

/** Helper function used to generate an empty buffer */
const emptyBuffer = (encoding: BufferEncoding = 'utf-8') => Buffer.from('', encoding)

/**
 * Data structure representing a single HTTP message, which could be either a request or a response.
 */
export class HttpMessage {
    protocol: string = 'HTTP/1.1'
    path: string = null
    host: string = null
    method: HttpMethod = null
    statusCode: number = null
    contentType: string = 'text/plain'
    headers: OutgoingHttpHeaders = {}
    cookies: SetCookie[] = []
    body: Buffer = emptyBuffer()
    json: any = {}
    // conn: WebSocketManager<HttpMessage>

    protected constructor() {}

    /** Parses a message body and returns an HttpMessage object */
    public static async parse(message: AwaitableHttpMessage): Promise<HttpMessage> {
        message = await Promise.resolve(message)
        if (message && isHttpMessage(message)) {
            return message as HttpMessage
        } else {
            return new HttpMessageBuilder().setBody(message).build()
        }
    }

    public static async fromRequest(request: IncomingMessage): Promise<HttpMessage> {
        const builder = new HttpMessageBuilder()
            .setHeaders(request.headers)
            .setHeader('path', request.url)
            .setHeader('host', request.headers.host)
            .setMethod(request.method as HttpMethod)
            .setContentType(request.headers['content-type'] || 'text/plain')

        builder.cookies = (request.headers['set-cookie'] || [])
            .map(setCookie => Cooky.parse(setCookie));  // TODO: cookie domain / pathname

        builder.setBody(await StreamAsync(request).readAsync())
        return builder.build()
    }

    protected safeParseJSON(body: string) {
        if (this.contentType === 'application/json') {
            try {
                return JSON.parse(body)
            } catch (err) {
                // No-op
            }
        }

        return {}
    }

    /**
     * Serializes the entire message. Note that this includes protocol, status code, etc. and those
     * fields will not be parsed when using the `parse` static method. 
     */
    public serialize() {
        const lines: string[] = []
        
        if (this.statusCode) {
            lines.push(`${this.protocol} ${this.statusCode}`)
        } else {
            lines.push(`${this.method} ${this.path || '/'} ${this.protocol}`)
        }

        for (const key in this.headers) {
            let val = this.headers[key]
            if (Array.isArray(val)) val = val.join('; ')
                lines.push(`${key}: ${val}`)
        }

        lines.push(`\n${this.body}`)
        return lines.join('\n')
    }
}

/**
 * Builder class for HttpMessage. This is the only safe way to create a HttpMessage. Properties
 * should not be set directly, instead use the provided setters.
 */
export class HttpMessageBuilder extends HttpMessage {

    constructor(base?: HttpMessage) {
        super()
        Object.assign(this, base)
    }

    /**
     * Sets the message path
     * @param path value to set this message path to
     */
    public setPath(path: string) {
        this.path = path
        this.delStatusCode();  // setting path erases status code
        return this
    }

    private delPath() {
        this.path = null
        delete this.headers['path']
        return this
    }

    /**
     * Sets the message host
     * @param host value to set this message host to
     */
    public setHost(host: string) {
        this.host = host
        this.delStatusCode();  // setting host erases status code
        return this
    }

    private delHost() {
        this.host = null
        delete this.headers['host']
        return this
    }

    public setMethod(method: HttpMethod) {
        this.method = method
        this.delStatusCode();  // setting method erases status code
        return this
    }

    private delMethod() {
        this.method = null
        // delete this.headers[':method']
        return this
    }

    /**
     * Sets the message status code
     * @param code value to set this message status code to
     */
    public setStatusCode(code: number) {
        if (!code || typeof code !== 'number')
            throw (`Invalid status code: ${code}`)
        this.statusCode = code
        this.delPath();  // setting status code erases path
        this.delHost();  // setting status code erases host
        this.delMethod();  // setting status code erases method
        return this
    }

    private delStatusCode() {
        this.statusCode = null
        // delete this.headers[':status']
        return this
    }

    /**
     * Sets the message body
     * @param data value to set this message data to
     */
    public setBody(data?: object | string | Buffer, autoparse: boolean = true) {

        // If there's no data, set body to empty buffer
        if (!data) data = emptyBuffer()

        // If the data is an object, try to serialize it as JSON
        if (autoparse && typeof data === 'object' && !Buffer.isBuffer(data)) {
            try {
                data = JSON.stringify(data)
                this.setContentType('application/json')

            } catch (exc) {
                console.error('Failed to set body:', data)
                data = emptyBuffer()
            }
        }

        // If data is a string, wrap it into a buffer
        if (typeof data === 'string') {
            try {
                data = Buffer.from(data, 'utf-8')
            } catch (exc) {
                console.error('Failed to set body:', data)
                data = emptyBuffer()
            }
        } 

        this.body = data as Buffer
        return this
    }

    /**
     * Sets the message content type
     * @param ctype value to set this message content type to
     */
    public setContentType(ctype: string) {
        if (!ctype || typeof ctype !== 'string')
            throw (`Invalid content type: ${ctype}`)
        this.contentType = ctype
        return this
    }

    /**
     * Sets or deletes a message header given its key
     * @param key key for the header
     * @param value value for the header, use `null` to delete the header
     */
    public setHeader(key: string, value: number | string | string[]) {
        key = key.toLocaleLowerCase()
        if (value === null) {
            // When value is null, delete the header
            delete this.headers[key]
        } else {
            // When value is anything else, set header to that value
            this.headers[key] = value
        }

        // Some headers require special treatment
        // if (key === ':method') {
        //     this.setMethod(value as HttpMethod)
        // }
        // if (key === ':status') {
        //     this.setStatusCode(value ? value as number : 200)
        // }
        // if (key === ':scheme') {
        //     this.url.protocol = `${value as string}:`
        // }
        // if (key === ':authority') {
        //     this.url.host = value as string
        // }
        if (key === 'path') {
            this.path = value as string
        }
        if (key === 'host') {
            this.host = value as string
        }
        if (key === 'content-type') {
            this.setContentType(value ? value as string : 'text/plain')
        }
        return this
    }

    /**
     * Sets all headers from object
     * @param headers key, value pairs of headers
     */
    public setHeaders(headers: OutgoingHttpHeaders) {
        for (const key in headers) {
            this.setHeader(key, headers[key])
        }
        return this
    }

    /**
     * Sets or deletes a message cookie given its key
     * @param key key for the cookie
     * @param value value for the cookie, use `null` to delete the cookie
     */
    public setCookie(key: string, value?: string, url?: string) {
        if (value === null) {
            // When value is null, delete the cookie
            this.cookies = this.cookies.filter(cookie => cookie.name !== key)
        } else {
            // When value is anything else, set cookie to that value
            for (const cookie of this.cookies) {
                if (cookie.name === key) {
                    cookie.value = value
                    return this
                }
            }
            // If no cookie was overwritten, add a new cookie
            this.cookies.push({url: url, name: key, value: value} as SetCookie)
        }

        return this
    }

    public build(): HttpMessage {
        // If neither method nor status code are set, assume status code should be 200
        if (!this.method && !this.statusCode) this.setStatusCode(200)

        // Try to parse body into a JSON object
        if (this.contentType === 'application/json') {
            this.json = this.safeParseJSON(this.body.toString('utf-8'))
        }

        // Set the headers according to internal properties
        if (this.protocol === 'HTTP/1.1' && this.path) {
            this.headers['path'] = this.path
        }
        if (this.protocol === 'HTTP/2' && this.path) {
            // this.headers[':authority'] = this.url.host
            // this.headers[':path'] = `${this.url.pathname}${this.url.hash || ''}${this.url.search}`
            // this.headers[':scheme'] = this.url.protocol.replace(':', '')
            this.headers[':path'] = this.path
        }
        if (this.protocol === 'HTTP/2' && this.method) {
            this.headers[':method'] = this.method
        }
        if (this.protocol === 'HTTP/2' && this.statusCode) {
            this.headers[':status'] = this.statusCode
        }
        if (this.cookies.length > 0) {
            this.headers['set-cookie'] = this.cookies.map(cookie => Cooky.stringify(cookie))
        }
        this.headers['content-type'] = this.contentType
        this.headers['content-length'] = this.body.byteLength

        // Return a new message with all the properties from this builder
        return Object.assign(new HttpMessage(), this)
    }
}


export abstract class URLFormatter {

    static fromAddress(address: AddressInfo | string, scheme: string = 'http') {
        if (typeof address === 'string') return address
        const host = (address.family === 'IPv4' ? address.address : `[${address.address}]`)
            .replace('0.0.0.0', '[::]').replace('[::]', 'localhost')
        return `${scheme}://${host}:${address.port}`
    }
}
