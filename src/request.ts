import * as WebSocket from 'ws'
import { StreamAsync } from 'stream-buffer-async'
import { Server as HttpServer, request as request_, OutgoingHttpHeaders } from 'http'
import { WebSocketManager } from './wsmanager'
import { HttpMessage, URLFormatter, HttpMessageBuilder } from './httpmessage'

abstract class BaseRequest {
    url: URL
    headers: OutgoingHttpHeaders = {}

    constructor(server: HttpServer | URL | string) {
        if (server instanceof HttpServer)
            this.url = new URL(URLFormatter.fromAddress(server.address()))
        if (server instanceof URL)
            this.url = server
        if (typeof server === 'string')
            this.url = new URL(server)
    }

    request(message: HttpMessage): Promise<HttpMessage> {
        throw ('Not implemented')
    }

    set(key: string, val: string | string[] | number) {
        this.headers[key] = val
        return this
    }

    get(path: string): Promise<HttpMessage> {
        return this.request(
            new HttpMessageBuilder().setMethod('GET').setPath(path).build())
    }

    post(path: string, data?: any): Promise<HttpMessage> {
        return this.request(
            new HttpMessageBuilder().setMethod('POST').setPath(path).setBody(data).build())
    }
}

/**
 * Asynchronous HTTP request helper methods
 */
export class HttpRequest extends BaseRequest {

    request(message: HttpMessage): Promise<HttpMessage> {
        const headers = Object.assign({}, this.headers, message.headers)

        const opts = Object.assign({}, message, {
            port: this.url.port,
            hostname: this.url.hostname,
            protocol: this.url.protocol,
            headers: headers
        })

        return new Promise<HttpMessage>((resolve, reject) => {
            const client = request_(opts, async res => {
                res.setEncoding('utf-8')
                const body = await StreamAsync(res).readAsync()
                resolve(new HttpMessageBuilder()
                    .setBody(body)
                    .setHeaders(res.headers)
                    .setStatusCode(res.statusCode)
                    .build())
            })

            client.write(message.body, 'utf-8', err => err && reject(err))
            client.end()
        })
    }
}

/**
 * Asynchronous websocket "request" methods. HTTP messages are serialized into data structure of
 * type `HttpMessage`. Receiver is expected to be able to parse that data structure using classes
 * and methods provided by this Node module.
 */
export class WebSocketRequest extends BaseRequest {
    manager: Promise<WebSocketManager>

    constructor(server: HttpServer | URL | string) {
        super(server)
        this.manager = WebSocketManager.create({url: this.url.href})
    }

    request(message: HttpMessage): Promise<HttpMessage> {
        const message_ = new HttpMessageBuilder(message).setHeaders(this.headers).build()
        return this.manager.then(m => m.request(message_))
    }
}