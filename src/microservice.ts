import * as moment from 'moment'
import * as sqlite from 'sqlite'
import { AddressInfo } from 'net'
import { ProcShell } from 'ootils'
import { setInterval } from 'timers'
import { Server as HttpServer } from 'http'
import { Server as WebSocketServer } from 'ws'
import { Router, RouteOptions, MatchOptions, RouteCallback, _defaultRouteOptions } from './routing'
import { HttpMessage, HttpMessageBuilder } from './httpmessage'


// Re-exports

export { Router, RouteOptions, MatchOptions, RouteCallback } from './routing'
export { HttpRequest, WebSocketRequest } from './request'
export { WebSocketManager } from './wsmanager'
export { HttpMethod, HttpMessage, HttpMessageBuilder, URLFormatter } from './httpmessage'


/**
 * Represents a self-contained service with its own routes, cache and backing storage
 */
export class MicroService {

    // Pointer to the backing storage used by this service
    public db: sqlite.Database
    // Pointer to the underlying Node HTTP server powering this service
    public server: HttpServer
    // Pointer to the WebSocket server living in the HTTP server
    public wss: WebSocketServer
    // Pointer to the underlying Express application powering this service
    public router: Router
    // Internal cleanup timer
    public maintenanceTimer: NodeJS.Timer
    // Internal fields
    public logExpiryDays: number
    public cacheExpirySeconds: number

    // Defeat instantiation
    private constructor() {}

    static create(dbfilename?: string, cacheExpirySeconds: number = 60, logExpiryDays: number = 30): Promise<MicroService> {
        return new Promise<MicroService>(async (resolve, reject) => {
            const service = new MicroService()

            // Database setup
            service.logExpiryDays = logExpiryDays
            service.cacheExpirySeconds = cacheExpirySeconds
            service.db = await sqlite.open(dbfilename || ':memory:')
            await service.db.run(`
                CREATE TABLE IF NOT EXISTS log (
                    level TEXT NOT NULL,
                    message TEXT NOT NULL,
                    timestamp DATETIME DEFAULT (STRFTIME('%Y-%m-%d %H:%M:%f', 'NOW')))`)
            await service.db.run(`PRAGMA journal_mode = WAL;`)
            await service.db.run(`PRAGMA busy_timeout = 15000;`)

            // Initialize cache
            await service.db.run(`
                CREATE TABLE IF NOT EXISTS cache (
                    url           TEXT NOT NULL,
                    headers_in    TEXT NOT NULL,
                    headers_out   TEXT NOT NULL,
                    body_in       TEXT NOT NULL,
                    body_out      TEXT NOT NULL,
                    is_json       INT,
                    status        INT,
                    timestamp     DATETIME DEFAULT (STRFTIME('%Y-%m-%d %H:%M:%f', 'NOW')))`)
            await service.clearCache()

            // Routing setup
            service.router = new Router()

            // Resolve with service now that database has been initialized
            resolve(service)
        })
    }

    /** Logs an event in the console as well as the internal database. Level can be E, W, I and V */
    async log(level: string, ...args: any[]) {
        if (level.length > 1) {
            args = [level].concat(args)
            level = 'I'
        }
        const fmt = 'YYYY-MM-DDTHH:mm:ss.SSS'
        console.log.apply(this, [moment().utc().format(fmt), level].concat(args))
        try {
            await this.db.run(
                `INSERT INTO log (level, message) VALUES (?, ?)`, [level, args.join('\t')])
        } catch (err) {
            console.log.apply(
                this, [moment().utc().format(fmt), 'E', 'Error updating log: ' + err.message])
        }

        return args.join(' ')
    }

    async clearLogs(all = false) {
        try {
            const prev  =`DATETIME('NOW', '-${this.logExpiryDays} days')`
            const cond = all ? '' : `WHERE timestamp < (STRFTIME('%Y-%m-%d %H:%M:%f', ${prev}))`
            await this.db.run(`DELETE FROM log ${cond}`)
        } catch (err) {
            this.log('E', err.message)
        }
    }

    async clearCache(all = false) {
        try {
            const prev  =`DATETIME('NOW', '-${this.cacheExpirySeconds} seconds')`
            const cond = all ? '' : `WHERE timestamp < (STRFTIME('%Y-%m-%d %H:%M:%f', ${prev}))`
            await this.db.run(`DELETE FROM cache ${cond}`)
        } catch (err) {
            this.log('E', err.message)
        }
    }

    queryCache(request: HttpMessage): Promise<HttpMessage> {
        return new Promise<HttpMessage>(async (resolve, reject) => {
            try {
                const prev  =`DATETIME('NOW', '-${this.cacheExpirySeconds} seconds')`
                const row = await this.db.get(`
                    SELECT * FROM cache WHERE url=(?) AND body_in=(?) AND headers_in=(?)
                    AND timestamp > (STRFTIME('%Y-%m-%d %H:%M:%f', ${prev})) LIMIT 1`,
                    [request.path, JSON.stringify(request.body), JSON.stringify(request.headers)])
                if (row && row.status !== 503) {
                    this.log('V', `[${row.status}] Serving result from cache`)
                    const body = Buffer.from(JSON.parse(row.body_out))
                    resolve(new HttpMessageBuilder()
                        .setPath(row.url)
                        .setHeaders(JSON.parse(row.headers_out))
                        .setStatusCode(row.status)
                        .setBody(row.is_json ? JSON.parse(body.toString('utf-8')) : body))
                } else {
                    resolve()
                }
            } catch (err) {
                this.log('E', err.message)
                resolve()
            }
        })
    }

    async updateCache(request: HttpMessage, response: HttpMessage) {
        try {
            await this.db.run(`
                INSERT INTO cache (url, status, body_in, body_out, is_json, headers_in, headers_out)
                VALUES (?, ?, ?, ?, ?, ?, ?)`, [
                    request.path, response.statusCode, 
                    JSON.stringify(request.body), JSON.stringify(response.body), response.json!!,
                    JSON.stringify(request.headers), JSON.stringify(response.headers)])
        } catch (err) {
            this.log('E', 'Error occurred updating cache: ' + err.message)
        }
    }

    private wrapCallback(options: RouteOptions, callback: RouteCallback): RouteCallback {

        // Log event and return wrapped function
        return async (request, params, conn) => {
            this.log('V', options.method, request.path)

            // Early exit: return from cache if available
            const data = await this.queryCache(request)
            if (data) return data

            // Perform actual callback
            const response = await HttpMessage.parse(callback(request, params, conn))
            this.updateCache(request, response)  // intentionally not awaited
            return response
        }
    }

    /**
     * Sets up a route for the MicroService to handle
     * @param options either a `RouteOptions` or a string with the path being registered
     * @param callback the callback function that will be performed by this route for every request
     */
    route(options: RouteOptions | string, callback: RouteCallback) {
        if (typeof options === 'string') options = {path: options}
        const options_: RouteOptions = Object.assign({}, _defaultRouteOptions, options)
        this.log('V', `Setting up ${options_.method} route for ${options_.path}`)

        // Wrap the callback to allow caching
        return this.router.set(options_, this.wrapCallback(options_, callback))
    }

    /**
     * Removes the specified route
     * @param options the MatchOptions returned from the call to `route()`
     */
    unroute(options: MatchOptions) {
        return this.router.remove(options)
    }

    /**
     * Signal other MicroService-aware components, such as SystemD services and
     * microservice-spawner that the service is ready.
     */
    notify(): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            // Signal parent process that server has started by disconnecting from the IPC channel
            if (process.connected) {
                process.disconnect()
            }
            // Signal systemd service (ignore if systemd-notify does not exist)
            ProcShell.which('systemd-notify').then(bin => {
                ProcShell.exec(`${bin} --ready`).catch(err => {
                    this.log('E', 'Unable to notify systemd daemon: ' + err.message)
                    reject(err)
                }).then(resolve)
            }).catch(err => resolve())
        })
    }

    start(port: number) {
        this.log('I', 'Starting microservice at port ' + port)

        // Open the HTTP server
        this.server = new HttpServer(this.router.httpRequestHandlerFactory())
        this.server.listen(port)

        // Open the web socket server
        this.wss = new WebSocketServer({server: this.server})
        this.wss.on('connection', this.router.webSocketHandler())

        // Setup error handlers
        this.server.on('error', err => this.log('E', err.message))
        this.wss.on('error', err => this.log('E', err.message))

        // Setup maintenance window to scrub logs and cache on a timely basis
        this.maintenanceTimer = setInterval(async () => {
            await this.clearLogs()
            await this.clearCache()
        }, 1 * 60 * 1000);  // Every minute
    }

    stop() {
        clearInterval(this.maintenanceTimer)
        if (this.server && this.server.listening) {
            this.server.removeAllListeners()
            this.server.close()
            this.server = null
        }
        if (this.wss) {
            this.wss.removeAllListeners()
            this.wss.close()
            this.wss = null
        }
    }

    port() {
        return this.server.listening && (this.server.address() as AddressInfo).port
    }
}
