
/**
 * From https://github.com/kimamula/TypeScript-definition-of-EventEmitter-with-keyof
 */
export interface ITypedEventEmitter<T> {
    listeners<K extends keyof T>(event: K): ((arg: T[K]) => any)[]
    addListener<K extends keyof T>(event: K, listener: (arg: T[K]) => any): this
    on<K extends keyof T>(event: K, listener: (arg: T[K]) => any): this
    once<K extends keyof T>(event: K, listener: (arg: T[K]) => any): this
    removeListener<K extends keyof T>(event: K, listener: (arg: T[K]) => any): this
    removeAllListeners<K extends keyof T>(event?: K): this
    // setMaxListeners(n: number): this
    // getMaxListeners(): number
    emit<K extends keyof T>(event: K, arg: T[K]): boolean
    listenerCount<K extends keyof T>(event: K): number
    prependListener<K extends keyof T>(event: K, listener: (arg: T[K]) => any): this
    // prependOnceListener<K extends keyof T>(event: K, listener: (arg: T[K]) => any): this
    // eventNames(): (string | symbol)[]
}

/**
 * Generic implementation of ITypedEventEmitter, to be subclassed for actual use
 */
export class TypedEventEmitter<T> implements ITypedEventEmitter<T> {
    private _listeners: Map<keyof T, ((arg: T[keyof T]) => any)[]>

    constructor() {
        this._listeners = new Map()
    }

    listeners<K extends keyof T>(event: K) {
        if (!this._listeners.has(event)) this._listeners.set(event, [])
        return this._listeners.get(event)
    }

    addListener<K extends keyof T>(event: K, listener: (arg: T[K]) => any) {
        this.listeners(event).push(listener)
        return this
    }

    prependListener<K extends keyof T>(event: K, listener: (arg: T[K]) => any) {
        this.listeners(event).unshift(listener)
        return this
    }

    removeListener<K extends keyof T>(event: K, listener: (arg: T[K]) => any) {
        const listeners = this.listeners(event)
        const idx = listeners.indexOf(listener)
        if (idx !== -1) listeners.splice(idx, 1)
        return this
    }

    removeAllListeners<K extends keyof T>(event?: K) {
        if (!event) {
            this._listeners = new Map()
        } else {
            this._listeners.delete(event)
        }
        return this
    }

    on<K extends keyof T>(event: K, listener: (arg: T[K]) => any) {
        return this.addListener(event, listener)
    }

    once<K extends keyof T>(event: K, listener: (arg: T[K]) => any) {
        const emitter = this
        return this.addListener(event, function callback(arg: T[K]) {
            emitter.removeListener(event, callback)
            listener(arg)
        })
    }

    emit<K extends keyof T>(event: K, arg: T[K]) {
        this.listeners(event).forEach(callback => callback(arg))
        return true
    }

    listenerCount<K extends keyof T>(event: K) {
        return this.listeners(event).length
    }
}
