import * as WebSocket from 'ws'
import { IncomingMessage, ServerResponse } from 'http'
import { WebSocketManager, PAYLOAD_KEY } from './wsmanager'
import { AwaitableHttpMessage, HttpMethod, HttpMessage, HttpMessageBuilder } from './httpmessage'

export type RouteParameters = {[key: string]: string}
export type RouteCallback = (message: HttpMessage, params?: RouteParameters,
    conn?: WebSocketManager) => AwaitableHttpMessage

export interface MatchOptions {
    expr: RegExp
    depth: number
    params: string[]
    method: HttpMethod
}

export interface RouteOptions {
    path: string
    method?: HttpMethod
    optionalPostParameters?: string[]
    mandatoryPostParameters?: string[]
    optionalQueryParameters?: string[]
    mandatoryQueryParameters?: string[]
}
export const _defaultRouteOptions: RouteOptions = {path: '/', method: 'GET'}

/**
 * Route matching and handling for HTTP connections
 */
export class Router {
    private invalidParameterName = /[^a-zA-Z0-9\-\.]/g
    private routes: Map<MatchOptions, RouteCallback> = new Map()
    private options: Map<MatchOptions, RouteOptions> = new Map()
    public constructor() {}

    private parse(options: RouteOptions) {
        if (options.path.startsWith('/'))
            options.path = options.path.substr(1)
        const parts = options.path.split('/')
        const params = parts.map(sub =>
            !sub.startsWith(':') ? null :
            // Sanitize parameter names by removing all invalid characters
            sub.substr(1).replace(this.invalidParameterName, ''))
        // TODO: this will not catch something invalid like /a/:b?/c
        const exprParts = parts.map(sub => {
            let expr = '\/'
            if (sub.endsWith('?')) expr += '?'
            expr += sub.startsWith(':') ? '([^\]*)' : sub
            if (sub.endsWith('?')) expr += '?'
            return expr
        })
        const exprRegex = RegExp(`^${exprParts.join('') || '\/'}$`)
        return {
            expr: exprRegex,
            depth: parts.length,
            params: params,
            method: options.method,
        } as MatchOptions
    }

    public set(options: RouteOptions, callback: RouteCallback) {
        const matcher = this.parse(options)
        this.routes.set(matcher, callback)
        this.options.set(matcher, options)
        return matcher
    }

    public remove(options: MatchOptions) {
        return this.routes.delete(options)
    }

    public async match(
            message: HttpMessage, conn?: WebSocketManager): Promise<HttpMessage> {
        // Iterate over all existing routes
        // TODO: use a hashmap instead?
        for (const [matcher, callback] of Array.of(...this.routes)) {

            // When we have a match, finish parsing and trigger callback
            const url = new URL(`http://localhost${message.path}`)
            if (matcher.method === message.method && url.pathname.match(matcher.expr)) {
                const options = this.options.get(matcher)

                // Make sure that the required parameters are passed
                const postdata = options.mandatoryPostParameters && message.json
                if (options.mandatoryPostParameters &&
                        !options.mandatoryPostParameters.every(param => param in postdata)) {
                    const paramcsv = options.mandatoryPostParameters.map(
                        param => `"${param}"`).join(', ')
                    const msg = `POST parameters ${paramcsv} are mandatory`
                    return new HttpMessageBuilder().setStatusCode(400).setBody(msg).build()
                }

                //const searchParams = new URL(`http://localhost${message.path}`).searchParams
                if (options.mandatoryQueryParameters && !options.mandatoryQueryParameters.every(
                        param => url.searchParams.has(param))) {
                    const paramcsv =
                        options.mandatoryQueryParameters.map(param => `"${param}"`).join(', ')
                    const msg = `Query parameters ${paramcsv} are mandatory`
                    return new HttpMessageBuilder().setStatusCode(400).setBody(msg).build()
                }

                // Parse parameters that can only be inferred during matching
                const parts = message.path.substr(1).split('/')
                const routeParams = parts.reduce((dict, sub, idx) => {
                    // Sanitize each potential parameter by removing querystring and hashes
                    sub = sub.replace(/[\?#].+/, '')
                    if (matcher.params[idx]) dict[matcher.params[idx]] = sub
                    return dict
                }, Object(null) as RouteParameters)

                // Trigger the callback to get response message
                try {
                    const response = await callback(message, routeParams, conn)
                    return HttpMessage.parse(response)
                } catch (exc) {
                    console.error(exc)
                    return new HttpMessageBuilder()
                        .setStatusCode(500)
                        .setBody('Server error: ' + exc.message)
                        .build()
                }
            }
        }

        // If we reached this point, then there were no matches
        return new HttpMessageBuilder().setStatusCode(404).build()
    }

    public httpRequestHandlerFactory() {
        return async (request: IncomingMessage, response: ServerResponse) => {
            // Convert the request to our own type and get result from callback
            const message = await this.match(await HttpMessageBuilder.fromRequest(request))

            // Send the result over the wire
            response.statusCode = message.statusCode
            Object.keys(message.headers).forEach(
                key => response.setHeader(key, message.headers[key]))

            return new Promise<void>(resolve => response.end(message.body, 'utf-8', resolve))
        }
    }

    public addWebSocketListener(manager: WebSocketManager) {
        manager.on('message', async request => {
            const response = await this.match(request, manager)
            manager.send(response, request[PAYLOAD_KEY])
        })
    }

    public webSocketHandler() {
        return async (socket: WebSocket) => {
            const manager = await WebSocketManager.create({socket: socket})
            this.addWebSocketListener(manager)
        }
    }
}