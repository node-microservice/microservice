const gulp = require('gulp');


// Clean tasks

gulp.task('clean', () => {
    const del = require('del');
    return del('dist');
});


// Build tasks

gulp.task('ts', () => {
    const ts = require('gulp-typescript');
    return gulp.src('src/**/*.ts')
        .pipe(ts({
            target: 'ES2015',
            module: 'commonjs',
            declaration: true,
            noImplicitAny: true,
         }))
        .pipe(gulp.dest('dist'));
});

gulp.task('copy', () => {
    return gulp.src('src/**/*.js')
        .pipe(gulp.dest('dist'));
})

gulp.task('build', gulp.series('ts', 'copy'));
gulp.task('default', gulp.series('build'));
