"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const wsmanager_1 = require("./wsmanager");
const httpmessage_1 = require("./httpmessage");
exports._defaultRouteOptions = { path: '/', method: 'GET' };
/**
 * Route matching and handling for HTTP connections
 */
class Router {
    constructor() {
        this.invalidParameterName = /[^a-zA-Z0-9\-\.]/g;
        this.routes = new Map();
        this.options = new Map();
    }
    parse(options) {
        if (options.path.startsWith('/'))
            options.path = options.path.substr(1);
        const parts = options.path.split('/');
        const params = parts.map(sub => !sub.startsWith(':') ? null :
            // Sanitize parameter names by removing all invalid characters
            sub.substr(1).replace(this.invalidParameterName, ''));
        // TODO: this will not catch something invalid like /a/:b?/c
        const exprParts = parts.map(sub => {
            let expr = '\/';
            if (sub.endsWith('?'))
                expr += '?';
            expr += sub.startsWith(':') ? '([^\]*)' : sub;
            if (sub.endsWith('?'))
                expr += '?';
            return expr;
        });
        const exprRegex = RegExp(`^${exprParts.join('') || '\/'}$`);
        return {
            expr: exprRegex,
            depth: parts.length,
            params: params,
            method: options.method,
        };
    }
    set(options, callback) {
        const matcher = this.parse(options);
        this.routes.set(matcher, callback);
        this.options.set(matcher, options);
        return matcher;
    }
    remove(options) {
        return this.routes.delete(options);
    }
    match(message, conn) {
        return __awaiter(this, void 0, void 0, function* () {
            // Iterate over all existing routes
            // TODO: use a hashmap instead?
            for (const [matcher, callback] of Array.of(...this.routes)) {
                // When we have a match, finish parsing and trigger callback
                const url = new URL(`http://localhost${message.path}`);
                if (matcher.method === message.method && url.pathname.match(matcher.expr)) {
                    const options = this.options.get(matcher);
                    // Make sure that the required parameters are passed
                    const postdata = options.mandatoryPostParameters && message.json;
                    if (options.mandatoryPostParameters &&
                        !options.mandatoryPostParameters.every(param => param in postdata)) {
                        const paramcsv = options.mandatoryPostParameters.map(param => `"${param}"`).join(', ');
                        const msg = `POST parameters ${paramcsv} are mandatory`;
                        return new httpmessage_1.HttpMessageBuilder().setStatusCode(400).setBody(msg).build();
                    }
                    //const searchParams = new URL(`http://localhost${message.path}`).searchParams
                    if (options.mandatoryQueryParameters && !options.mandatoryQueryParameters.every(param => url.searchParams.has(param))) {
                        const paramcsv = options.mandatoryQueryParameters.map(param => `"${param}"`).join(', ');
                        const msg = `Query parameters ${paramcsv} are mandatory`;
                        return new httpmessage_1.HttpMessageBuilder().setStatusCode(400).setBody(msg).build();
                    }
                    // Parse parameters that can only be inferred during matching
                    const parts = message.path.substr(1).split('/');
                    const routeParams = parts.reduce((dict, sub, idx) => {
                        // Sanitize each potential parameter by removing querystring and hashes
                        sub = sub.replace(/[\?#].+/, '');
                        if (matcher.params[idx])
                            dict[matcher.params[idx]] = sub;
                        return dict;
                    }, Object(null));
                    // Trigger the callback to get response message
                    try {
                        const response = yield callback(message, routeParams, conn);
                        return httpmessage_1.HttpMessage.parse(response);
                    }
                    catch (exc) {
                        console.error(exc);
                        return new httpmessage_1.HttpMessageBuilder()
                            .setStatusCode(500)
                            .setBody('Server error: ' + exc.message)
                            .build();
                    }
                }
            }
            // If we reached this point, then there were no matches
            return new httpmessage_1.HttpMessageBuilder().setStatusCode(404).build();
        });
    }
    httpRequestHandlerFactory() {
        return (request, response) => __awaiter(this, void 0, void 0, function* () {
            // Convert the request to our own type and get result from callback
            const message = yield this.match(yield httpmessage_1.HttpMessageBuilder.fromRequest(request));
            // Send the result over the wire
            response.statusCode = message.statusCode;
            Object.keys(message.headers).forEach(key => response.setHeader(key, message.headers[key]));
            return new Promise(resolve => response.end(message.body, 'utf-8', resolve));
        });
    }
    addWebSocketListener(manager) {
        manager.on('message', (request) => __awaiter(this, void 0, void 0, function* () {
            const response = yield this.match(request, manager);
            manager.send(response, request[wsmanager_1.PAYLOAD_KEY]);
        }));
    }
    webSocketHandler() {
        return (socket) => __awaiter(this, void 0, void 0, function* () {
            const manager = yield wsmanager_1.WebSocketManager.create({ socket: socket });
            this.addWebSocketListener(manager);
        });
    }
}
exports.Router = Router;
