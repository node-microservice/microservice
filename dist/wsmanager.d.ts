import * as WebSocket from 'ws';
import { HttpMessage } from './httpmessage';
import { TypedEventEmitter } from './events';
export declare const PAYLOAD_KEY = "_payload_id";
export declare type Payload = HttpMessage & {
    [PAYLOAD_KEY]: number;
};
export declare type MessageListener = (message: HttpMessage) => void;
export interface WebSocketEvents {
    close: void;
    error: Error;
    message: Payload;
}
/**
 * Object that takes a websocket during initialization and performs automatic serialization and
 * deserialization of messages. Also keeps track of request-reponse by adding a payload ID to
 * messages sent.
 */
export declare class WebSocketManager extends TypedEventEmitter<WebSocketEvents> {
    readonly id: number;
    readonly url: string;
    readonly socket: WebSocket;
    protected constructor(url: string, socket: WebSocket);
    static create(options?: {
        url?: string;
        socket?: WebSocket;
    }): Promise<WebSocketManager>;
    close(): void;
    send(message: HttpMessage, payload_id?: number, timeout?: number): Promise<void>;
    read(payload_id?: number, timeout?: number): Promise<HttpMessage>;
    request(message: HttpMessage): Promise<HttpMessage>;
}
