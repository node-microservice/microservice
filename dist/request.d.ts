/// <reference types="node" />
import { Server as HttpServer, OutgoingHttpHeaders } from 'http';
import { WebSocketManager } from './wsmanager';
import { HttpMessage } from './httpmessage';
declare abstract class BaseRequest {
    url: URL;
    headers: OutgoingHttpHeaders;
    constructor(server: HttpServer | URL | string);
    request(message: HttpMessage): Promise<HttpMessage>;
    set(key: string, val: string | string[] | number): this;
    get(path: string): Promise<HttpMessage>;
    post(path: string, data?: any): Promise<HttpMessage>;
}
/**
 * Asynchronous HTTP request helper methods
 */
export declare class HttpRequest extends BaseRequest {
    request(message: HttpMessage): Promise<HttpMessage>;
}
/**
 * Asynchronous websocket "request" methods. HTTP messages are serialized into data structure of
 * type `HttpMessage`. Receiver is expected to be able to parse that data structure using classes
 * and methods provided by this Node module.
 */
export declare class WebSocketRequest extends BaseRequest {
    manager: Promise<WebSocketManager>;
    constructor(server: HttpServer | URL | string);
    request(message: HttpMessage): Promise<HttpMessage>;
}
export {};
