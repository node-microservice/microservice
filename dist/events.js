"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Generic implementation of ITypedEventEmitter, to be subclassed for actual use
 */
class TypedEventEmitter {
    constructor() {
        this._listeners = new Map();
    }
    listeners(event) {
        if (!this._listeners.has(event))
            this._listeners.set(event, []);
        return this._listeners.get(event);
    }
    addListener(event, listener) {
        this.listeners(event).push(listener);
        return this;
    }
    prependListener(event, listener) {
        this.listeners(event).unshift(listener);
        return this;
    }
    removeListener(event, listener) {
        const listeners = this.listeners(event);
        const idx = listeners.indexOf(listener);
        if (idx !== -1)
            listeners.splice(idx, 1);
        return this;
    }
    removeAllListeners(event) {
        if (!event) {
            this._listeners = new Map();
        }
        else {
            this._listeners.delete(event);
        }
        return this;
    }
    on(event, listener) {
        return this.addListener(event, listener);
    }
    once(event, listener) {
        const emitter = this;
        return this.addListener(event, function callback(arg) {
            emitter.removeListener(event, callback);
            listener(arg);
        });
    }
    emit(event, arg) {
        this.listeners(event).forEach(callback => callback(arg));
        return true;
    }
    listenerCount(event) {
        return this.listeners(event).length;
    }
}
exports.TypedEventEmitter = TypedEventEmitter;
