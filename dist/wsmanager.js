"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const WebSocket = require("ws");
const httpmessage_1 = require("./httpmessage");
const events_1 = require("./events");
exports.PAYLOAD_KEY = '_payload_id';
// Helper function used to generate random IDs with low likelihood of collision
const id = () => Date.now() + parseInt(String(Math.random()).substr(2, 4));
/**
 * Object that takes a websocket during initialization and performs automatic serialization and
 * deserialization of messages. Also keeps track of request-reponse by adding a payload ID to
 * messages sent.
 */
class WebSocketManager extends events_1.TypedEventEmitter {
    constructor(url, socket) {
        super();
        this.id = id();
        this.url = url;
        this.socket = socket;
    }
    static create(options = {}) {
        return __awaiter(this, void 0, void 0, function* () {
            options = Object.assign({}, options);
            // Early exit: we have no socket or URL to manage
            if (!options.socket && !options.url) {
                throw new Error('Either a socket or a URL must be provided');
            }
            // Instantiate manager and associate it with a socket
            const socket = options.socket || new WebSocket(options.url);
            const url = options.url || socket.url;
            const manager = new WebSocketManager(url, socket);
            // Setup an error handler to prevent crashing of main process
            socket.on('error', () => { });
            socket.once('error', () => manager.close());
            // Depending on the state of the websocket, we may need to wait until connection opens
            if (socket.readyState === WebSocket.OPEN) {
                // No-op
            }
            else if (socket.readyState === WebSocket.CONNECTING) {
                yield new Promise((resolve, reject) => {
                    setTimeout(() => reject(new Error('Timeout')), 5000);
                    socket.once('open', resolve);
                });
            }
            else if (socket.readyState === WebSocket.CLOSED) {
                throw new Error('socket already closed');
            }
            // Setup a global message listener that parses incoming data into Payload and then
            // calls our internal listeners in the order they were set
            socket.on('message', (data) => {
                try {
                    // Parse the incoming message as a Payload object
                    const message_in = JSON.parse(data.toString());
                    // The body will get incorrectly parsed as JSON, so we have to convert to Buffer
                    let body = Buffer.from(message_in.body);
                    // If the body of the message is of JSON type, we have to parse it too
                    if (message_in.contentType === 'application/json') {
                        body = JSON.parse(body.toString('utf-8'));
                    }
                    // Now we re-build the message that has been properly deserialized
                    const message_out = new httpmessage_1.HttpMessageBuilder(message_in)
                        .setBody(body)
                        .build();
                    // Finally we can send the message to all listeners
                    manager.emit('message', message_out);
                }
                catch (exc) {
                    manager.emit('error', exc);
                }
            });
            // If we are being asked to reconnect, listen to the close event and reopen the connection
            // FIXME: For now, when the socket closes then so does the socket manager
            socket.on('close', () => manager.close());
            return manager;
        });
    }
    close() {
        try {
            this.socket.close();
        }
        catch (exc) {
            // No-op
        }
        this.emit('close', void 0);
    }
    send(message, payload_id = null, timeout = 60000) {
        const payload = Object.assign({}, message, { [exports.PAYLOAD_KEY]: payload_id });
        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            setTimeout(() => reject(new Error('Timeout')), timeout);
            this.socket.send(JSON.stringify(payload), err => err && reject(err));
            resolve();
        }));
    }
    read(payload_id = null, timeout = 60000) {
        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            setTimeout(() => reject(new Error('timeout')), timeout);
            const emitter = this;
            this.on('message', function callback(message) {
                if (message[exports.PAYLOAD_KEY] === payload_id) {
                    emitter.removeListener('message', callback);
                    resolve(message);
                }
            });
        }));
    }
    request(message) {
        const payload_id = id();
        return new Promise((resolve, reject) => {
            this.read(payload_id).then(resolve).catch(reject);
            this.send(message, payload_id).catch(reject);
        });
    }
}
exports.WebSocketManager = WebSocketManager;
