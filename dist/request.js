"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const stream_buffer_async_1 = require("stream-buffer-async");
const http_1 = require("http");
const wsmanager_1 = require("./wsmanager");
const httpmessage_1 = require("./httpmessage");
class BaseRequest {
    constructor(server) {
        this.headers = {};
        if (server instanceof http_1.Server)
            this.url = new URL(httpmessage_1.URLFormatter.fromAddress(server.address()));
        if (server instanceof URL)
            this.url = server;
        if (typeof server === 'string')
            this.url = new URL(server);
    }
    request(message) {
        throw ('Not implemented');
    }
    set(key, val) {
        this.headers[key] = val;
        return this;
    }
    get(path) {
        return this.request(new httpmessage_1.HttpMessageBuilder().setMethod('GET').setPath(path).build());
    }
    post(path, data) {
        return this.request(new httpmessage_1.HttpMessageBuilder().setMethod('POST').setPath(path).setBody(data).build());
    }
}
/**
 * Asynchronous HTTP request helper methods
 */
class HttpRequest extends BaseRequest {
    request(message) {
        const headers = Object.assign({}, this.headers, message.headers);
        const opts = Object.assign({}, message, {
            port: this.url.port,
            hostname: this.url.hostname,
            protocol: this.url.protocol,
            headers: headers
        });
        return new Promise((resolve, reject) => {
            const client = http_1.request(opts, (res) => __awaiter(this, void 0, void 0, function* () {
                res.setEncoding('utf-8');
                const body = yield stream_buffer_async_1.StreamAsync(res).readAsync();
                resolve(new httpmessage_1.HttpMessageBuilder()
                    .setBody(body)
                    .setHeaders(res.headers)
                    .setStatusCode(res.statusCode)
                    .build());
            }));
            client.write(message.body, 'utf-8', err => err && reject(err));
            client.end();
        });
    }
}
exports.HttpRequest = HttpRequest;
/**
 * Asynchronous websocket "request" methods. HTTP messages are serialized into data structure of
 * type `HttpMessage`. Receiver is expected to be able to parse that data structure using classes
 * and methods provided by this Node module.
 */
class WebSocketRequest extends BaseRequest {
    constructor(server) {
        super(server);
        this.manager = wsmanager_1.WebSocketManager.create({ url: this.url.href });
    }
    request(message) {
        const message_ = new httpmessage_1.HttpMessageBuilder(message).setHeaders(this.headers).build();
        return this.manager.then(m => m.request(message_));
    }
}
exports.WebSocketRequest = WebSocketRequest;
