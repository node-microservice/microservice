/// <reference types="node" />
import { OutgoingHttpHeaders, IncomingMessage } from 'http';
import { AddressInfo } from 'net';
import { SetCookie } from './cooky';
export declare type HttpMethod = 'DELETE' | 'GET' | 'PATCH' | 'POST' | 'PUT';
export declare type ParseableHttpMessage = HttpMessage | string | object;
export declare type AwaitableHttpMessage = ParseableHttpMessage | Promise<ParseableHttpMessage>;
/**
 * Data structure representing a single HTTP message, which could be either a request or a response.
 */
export declare class HttpMessage {
    protocol: string;
    path: string;
    host: string;
    method: HttpMethod;
    statusCode: number;
    contentType: string;
    headers: OutgoingHttpHeaders;
    cookies: SetCookie[];
    body: Buffer;
    json: any;
    protected constructor();
    /** Parses a message body and returns an HttpMessage object */
    static parse(message: AwaitableHttpMessage): Promise<HttpMessage>;
    static fromRequest(request: IncomingMessage): Promise<HttpMessage>;
    protected safeParseJSON(body: string): any;
    /**
     * Serializes the entire message. Note that this includes protocol, status code, etc. and those
     * fields will not be parsed when using the `parse` static method.
     */
    serialize(): string;
}
/**
 * Builder class for HttpMessage. This is the only safe way to create a HttpMessage. Properties
 * should not be set directly, instead use the provided setters.
 */
export declare class HttpMessageBuilder extends HttpMessage {
    constructor(base?: HttpMessage);
    /**
     * Sets the message path
     * @param path value to set this message path to
     */
    setPath(path: string): this;
    private delPath;
    /**
     * Sets the message host
     * @param host value to set this message host to
     */
    setHost(host: string): this;
    private delHost;
    setMethod(method: HttpMethod): this;
    private delMethod;
    /**
     * Sets the message status code
     * @param code value to set this message status code to
     */
    setStatusCode(code: number): this;
    private delStatusCode;
    /**
     * Sets the message body
     * @param data value to set this message data to
     */
    setBody(data?: object | string | Buffer, autoparse?: boolean): this;
    /**
     * Sets the message content type
     * @param ctype value to set this message content type to
     */
    setContentType(ctype: string): this;
    /**
     * Sets or deletes a message header given its key
     * @param key key for the header
     * @param value value for the header, use `null` to delete the header
     */
    setHeader(key: string, value: number | string | string[]): this;
    /**
     * Sets all headers from object
     * @param headers key, value pairs of headers
     */
    setHeaders(headers: OutgoingHttpHeaders): this;
    /**
     * Sets or deletes a message cookie given its key
     * @param key key for the cookie
     * @param value value for the cookie, use `null` to delete the cookie
     */
    setCookie(key: string, value?: string, url?: string): this;
    build(): HttpMessage;
}
export declare abstract class URLFormatter {
    static fromAddress(address: AddressInfo | string, scheme?: string): string;
}
