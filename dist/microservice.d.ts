/// <reference types="node" />
import * as sqlite from 'sqlite';
import { Server as HttpServer } from 'http';
import { Server as WebSocketServer } from 'ws';
import { Router, RouteOptions, MatchOptions, RouteCallback } from './routing';
import { HttpMessage } from './httpmessage';
export { Router, RouteOptions, MatchOptions, RouteCallback } from './routing';
export { HttpRequest, WebSocketRequest } from './request';
export { WebSocketManager } from './wsmanager';
export { HttpMethod, HttpMessage, HttpMessageBuilder, URLFormatter } from './httpmessage';
/**
 * Represents a self-contained service with its own routes, cache and backing storage
 */
export declare class MicroService {
    db: sqlite.Database;
    server: HttpServer;
    wss: WebSocketServer;
    router: Router;
    maintenanceTimer: NodeJS.Timer;
    logExpiryDays: number;
    cacheExpirySeconds: number;
    private constructor();
    static create(dbfilename?: string, cacheExpirySeconds?: number, logExpiryDays?: number): Promise<MicroService>;
    /** Logs an event in the console as well as the internal database. Level can be E, W, I and V */
    log(level: string, ...args: any[]): Promise<string>;
    clearLogs(all?: boolean): Promise<void>;
    clearCache(all?: boolean): Promise<void>;
    queryCache(request: HttpMessage): Promise<HttpMessage>;
    updateCache(request: HttpMessage, response: HttpMessage): Promise<void>;
    private wrapCallback;
    /**
     * Sets up a route for the MicroService to handle
     * @param options either a `RouteOptions` or a string with the path being registered
     * @param callback the callback function that will be performed by this route for every request
     */
    route(options: RouteOptions | string, callback: RouteCallback): MatchOptions;
    /**
     * Removes the specified route
     * @param options the MatchOptions returned from the call to `route()`
     */
    unroute(options: MatchOptions): boolean;
    /**
     * Signal other MicroService-aware components, such as SystemD services and
     * microservice-spawner that the service is ready.
     */
    notify(): Promise<void>;
    start(port: number): void;
    stop(): void;
    port(): number;
}
