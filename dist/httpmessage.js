"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const stream_buffer_async_1 = require("stream-buffer-async");
const cooky_1 = require("./cooky");
function isHttpMessage(message) {
    const props = ['protocol', 'path', 'method', 'statusCode', 'contentType', 'headers', 'cookies',
        'body', 'json'];
    return props.every(key => message.hasOwnProperty(key));
}
/** Helper function used to generate an empty buffer */
const emptyBuffer = (encoding = 'utf-8') => Buffer.from('', encoding);
/**
 * Data structure representing a single HTTP message, which could be either a request or a response.
 */
class HttpMessage {
    // conn: WebSocketManager<HttpMessage>
    constructor() {
        this.protocol = 'HTTP/1.1';
        this.path = null;
        this.host = null;
        this.method = null;
        this.statusCode = null;
        this.contentType = 'text/plain';
        this.headers = {};
        this.cookies = [];
        this.body = emptyBuffer();
        this.json = {};
    }
    /** Parses a message body and returns an HttpMessage object */
    static parse(message) {
        return __awaiter(this, void 0, void 0, function* () {
            message = yield Promise.resolve(message);
            if (message && isHttpMessage(message)) {
                return message;
            }
            else {
                return new HttpMessageBuilder().setBody(message).build();
            }
        });
    }
    static fromRequest(request) {
        return __awaiter(this, void 0, void 0, function* () {
            const builder = new HttpMessageBuilder()
                .setHeaders(request.headers)
                .setHeader('path', request.url)
                .setHeader('host', request.headers.host)
                .setMethod(request.method)
                .setContentType(request.headers['content-type'] || 'text/plain');
            builder.cookies = (request.headers['set-cookie'] || [])
                .map(setCookie => cooky_1.Cooky.parse(setCookie)); // TODO: cookie domain / pathname
            builder.setBody(yield stream_buffer_async_1.StreamAsync(request).readAsync());
            return builder.build();
        });
    }
    safeParseJSON(body) {
        if (this.contentType === 'application/json') {
            try {
                return JSON.parse(body);
            }
            catch (err) {
                // No-op
            }
        }
        return {};
    }
    /**
     * Serializes the entire message. Note that this includes protocol, status code, etc. and those
     * fields will not be parsed when using the `parse` static method.
     */
    serialize() {
        const lines = [];
        if (this.statusCode) {
            lines.push(`${this.protocol} ${this.statusCode}`);
        }
        else {
            lines.push(`${this.method} ${this.path || '/'} ${this.protocol}`);
        }
        for (const key in this.headers) {
            let val = this.headers[key];
            if (Array.isArray(val))
                val = val.join('; ');
            lines.push(`${key}: ${val}`);
        }
        lines.push(`\n${this.body}`);
        return lines.join('\n');
    }
}
exports.HttpMessage = HttpMessage;
/**
 * Builder class for HttpMessage. This is the only safe way to create a HttpMessage. Properties
 * should not be set directly, instead use the provided setters.
 */
class HttpMessageBuilder extends HttpMessage {
    constructor(base) {
        super();
        Object.assign(this, base);
    }
    /**
     * Sets the message path
     * @param path value to set this message path to
     */
    setPath(path) {
        this.path = path;
        this.delStatusCode(); // setting path erases status code
        return this;
    }
    delPath() {
        this.path = null;
        delete this.headers['path'];
        return this;
    }
    /**
     * Sets the message host
     * @param host value to set this message host to
     */
    setHost(host) {
        this.host = host;
        this.delStatusCode(); // setting host erases status code
        return this;
    }
    delHost() {
        this.host = null;
        delete this.headers['host'];
        return this;
    }
    setMethod(method) {
        this.method = method;
        this.delStatusCode(); // setting method erases status code
        return this;
    }
    delMethod() {
        this.method = null;
        // delete this.headers[':method']
        return this;
    }
    /**
     * Sets the message status code
     * @param code value to set this message status code to
     */
    setStatusCode(code) {
        if (!code || typeof code !== 'number')
            throw (`Invalid status code: ${code}`);
        this.statusCode = code;
        this.delPath(); // setting status code erases path
        this.delHost(); // setting status code erases host
        this.delMethod(); // setting status code erases method
        return this;
    }
    delStatusCode() {
        this.statusCode = null;
        // delete this.headers[':status']
        return this;
    }
    /**
     * Sets the message body
     * @param data value to set this message data to
     */
    setBody(data, autoparse = true) {
        // If there's no data, set body to empty buffer
        if (!data)
            data = emptyBuffer();
        // If the data is an object, try to serialize it as JSON
        if (autoparse && typeof data === 'object' && !Buffer.isBuffer(data)) {
            try {
                data = JSON.stringify(data);
                this.setContentType('application/json');
            }
            catch (exc) {
                console.error('Failed to set body:', data);
                data = emptyBuffer();
            }
        }
        // If data is a string, wrap it into a buffer
        if (typeof data === 'string') {
            try {
                data = Buffer.from(data, 'utf-8');
            }
            catch (exc) {
                console.error('Failed to set body:', data);
                data = emptyBuffer();
            }
        }
        this.body = data;
        return this;
    }
    /**
     * Sets the message content type
     * @param ctype value to set this message content type to
     */
    setContentType(ctype) {
        if (!ctype || typeof ctype !== 'string')
            throw (`Invalid content type: ${ctype}`);
        this.contentType = ctype;
        return this;
    }
    /**
     * Sets or deletes a message header given its key
     * @param key key for the header
     * @param value value for the header, use `null` to delete the header
     */
    setHeader(key, value) {
        key = key.toLocaleLowerCase();
        if (value === null) {
            // When value is null, delete the header
            delete this.headers[key];
        }
        else {
            // When value is anything else, set header to that value
            this.headers[key] = value;
        }
        // Some headers require special treatment
        // if (key === ':method') {
        //     this.setMethod(value as HttpMethod)
        // }
        // if (key === ':status') {
        //     this.setStatusCode(value ? value as number : 200)
        // }
        // if (key === ':scheme') {
        //     this.url.protocol = `${value as string}:`
        // }
        // if (key === ':authority') {
        //     this.url.host = value as string
        // }
        if (key === 'path') {
            this.path = value;
        }
        if (key === 'host') {
            this.host = value;
        }
        if (key === 'content-type') {
            this.setContentType(value ? value : 'text/plain');
        }
        return this;
    }
    /**
     * Sets all headers from object
     * @param headers key, value pairs of headers
     */
    setHeaders(headers) {
        for (const key in headers) {
            this.setHeader(key, headers[key]);
        }
        return this;
    }
    /**
     * Sets or deletes a message cookie given its key
     * @param key key for the cookie
     * @param value value for the cookie, use `null` to delete the cookie
     */
    setCookie(key, value, url) {
        if (value === null) {
            // When value is null, delete the cookie
            this.cookies = this.cookies.filter(cookie => cookie.name !== key);
        }
        else {
            // When value is anything else, set cookie to that value
            for (const cookie of this.cookies) {
                if (cookie.name === key) {
                    cookie.value = value;
                    return this;
                }
            }
            // If no cookie was overwritten, add a new cookie
            this.cookies.push({ url: url, name: key, value: value });
        }
        return this;
    }
    build() {
        // If neither method nor status code are set, assume status code should be 200
        if (!this.method && !this.statusCode)
            this.setStatusCode(200);
        // Try to parse body into a JSON object
        if (this.contentType === 'application/json') {
            this.json = this.safeParseJSON(this.body.toString('utf-8'));
        }
        // Set the headers according to internal properties
        if (this.protocol === 'HTTP/1.1' && this.path) {
            this.headers['path'] = this.path;
        }
        if (this.protocol === 'HTTP/2' && this.path) {
            // this.headers[':authority'] = this.url.host
            // this.headers[':path'] = `${this.url.pathname}${this.url.hash || ''}${this.url.search}`
            // this.headers[':scheme'] = this.url.protocol.replace(':', '')
            this.headers[':path'] = this.path;
        }
        if (this.protocol === 'HTTP/2' && this.method) {
            this.headers[':method'] = this.method;
        }
        if (this.protocol === 'HTTP/2' && this.statusCode) {
            this.headers[':status'] = this.statusCode;
        }
        if (this.cookies.length > 0) {
            this.headers['set-cookie'] = this.cookies.map(cookie => cooky_1.Cooky.stringify(cookie));
        }
        this.headers['content-type'] = this.contentType;
        this.headers['content-length'] = this.body.byteLength;
        // Return a new message with all the properties from this builder
        return Object.assign(new HttpMessage(), this);
    }
}
exports.HttpMessageBuilder = HttpMessageBuilder;
class URLFormatter {
    static fromAddress(address, scheme = 'http') {
        if (typeof address === 'string')
            return address;
        const host = (address.family === 'IPv4' ? address.address : `[${address.address}]`)
            .replace('0.0.0.0', '[::]').replace('[::]', 'localhost');
        return `${scheme}://${host}:${address.port}`;
    }
}
exports.URLFormatter = URLFormatter;
