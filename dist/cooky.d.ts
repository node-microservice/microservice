export interface SetCookie {
    url: string;
    name: string;
    value: string;
    domain?: string;
    expires?: number;
    httpOnly: boolean;
    path?: string;
    sameSite?: boolean;
    secure?: boolean;
}
export declare class Cooky {
    static parse(setCookie: string, url?: string): SetCookie;
    static stringify(cookie: SetCookie): string;
}
