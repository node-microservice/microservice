"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const assert = require("assert");
// import * as request from 'supertest'
const request_1 = require("../request");
const microservice_1 = require("../microservice");
const json = { "headlines": [{ "description": "Authorities say it's unclear what started the fire at the popular tourist attraction." }, { "description": "The bike, made by BigRep, took three days to print and cost about Â£2,000 ($2,600) to produce." }, { "description": "No women featured in the announcement celebrating the Gulf state's efforts to reduce the gender gap." }, { "description": "Trevor, the only duck on the Pacific island of Niue, lived in a puddle and was cared for by locals." }, { "description": "An etiquette course in an Istanbul municipality advises women not to lick ice cream." }, { "description": "An Italian policeman runs a race in Siberia braving temperatures of -52 Celsius." }, { "description": "Climate change could make South Korea a banana producer." }, { "description": "A look back at photojournalist Peter Dench's journey on the Trans-Siberian Railway during the World Cup in Russia 2018." }, { "description": "A controversial new abortion law has been enacted in New York state, but why has it caused such a heated debate?" }] };
describe('microservice', () => {
    let service;
    const requestTypes = ['HttpRequest', 'WebSocketRequest'];
    before(() => __awaiter(void 0, void 0, void 0, function* () {
        service = yield microservice_1.MicroService.create();
        service.route('/', (_) => __awaiter(void 0, void 0, void 0, function* () { return new microservice_1.HttpMessageBuilder().setBody('Hello World').build(); }));
        service.route('/object', (_) => __awaiter(void 0, void 0, void 0, function* () { return new microservice_1.HttpMessageBuilder().setBody({ 0: Math.random() }).build(); }));
        service.route('/random', (_) => __awaiter(void 0, void 0, void 0, function* () { return new microservice_1.HttpMessageBuilder().setBody('' + Math.random()).build(); }));
        service.route({ path: '/params', method: 'GET', mandatoryQueryParameters: ['test'] }, (_) => __awaiter(void 0, void 0, void 0, function* () { return new microservice_1.HttpMessageBuilder().setBody('' + Math.random()).build(); }));
        service.route('/object_raw', () => __awaiter(void 0, void 0, void 0, function* () { return new microservice_1.HttpMessageBuilder().setBody(json).build(); }));
    }));
    beforeEach(() => __awaiter(void 0, void 0, void 0, function* () {
        service.stop();
        yield service.clearCache();
        service.start(Math.floor(Math.random() * (9999 - 1024)) + 1024);
    }));
    afterEach(() => {
        service.stop();
    });
    requestTypes.forEach(name => {
        let request;
        if (name === 'HttpRequest')
            request = () => new request_1.HttpRequest(service.server);
        if (name === 'WebSocketRequest')
            request = () => new request_1.WebSocketRequest(service.server);
        describe(`${name} cache`, () => {
            it('records incoming request', () => __awaiter(void 0, void 0, void 0, function* () {
                const path = '/?q=' + Math.random().toString();
                const res = yield request().get(path);
                const cache = yield service.db.get(`
                    SELECT * FROM cache
                    ORDER BY timestamp DESC LIMIT 1`);
                assert.equal(cache.url, path);
            }));
            it('repeats consecutive requests', () => __awaiter(void 0, void 0, void 0, function* () {
                const path = '/random';
                const res1 = yield request().get(path);
                const res2 = yield request().get(path);
                assert.equal(res1.body.toString(), res2.body.toString());
            }));
            it('expires', () => __awaiter(void 0, void 0, void 0, function* () {
                const path = '/random';
                const seconds = service.cacheExpirySeconds;
                service.cacheExpirySeconds = 0;
                const res1 = yield request().get(path);
                yield new Promise((resolve, _) => setTimeout(resolve, 1000));
                const res2 = yield request().get(path);
                assert.notEqual(res1.body, res2.body);
                service.cacheExpirySeconds = seconds;
            }));
            it('converts non-string output', () => __awaiter(void 0, void 0, void 0, function* () {
                const path = '/object';
                const res1 = yield request().get(path);
                const cache = yield service.db.get(`
                    SELECT * FROM cache
                    ORDER BY timestamp DESC LIMIT 1`);
                const res2 = yield request().get(path);
                // console.log(res1, res2, cache)
                assert.deepEqual(res1.json, res2.json, 'Compare body JSON');
                assert.deepEqual(res1.body.toJSON(), res2.body.toJSON(), 'Compare body Buffers');
                assert.deepEqual(JSON.parse(cache.body_out), res1.body.toJSON(), 'Compare with cache');
            }));
            it('handles utf-8 text', () => __awaiter(void 0, void 0, void 0, function* () {
                // Setup routes for this test
                const rnd = String(Math.random()).substr(2);
                const path = `/${rnd}`;
                const route = service.route({ path: path, method: 'GET' }, () => __awaiter(void 0, void 0, void 0, function* () { return new microservice_1.HttpMessageBuilder().setBody('的').build(); }));
                const res1 = yield request().get(path);
                const cache = yield service.db.get(`
                    SELECT * FROM cache
                    ORDER BY timestamp DESC LIMIT 1`);
                const res2 = yield request().get(path);
                assert.equal(res1.body.toString('utf-8'), res2.body.toString('utf-8'));
                assert.deepEqual(Buffer.from(JSON.parse(cache.body_out)).toJSON(), res1.body.toJSON());
                // Clear routes created by this test
                service.unroute(route);
            }));
            it('does not cache different headers', () => __awaiter(void 0, void 0, void 0, function* () {
                const path = '/random';
                const res1 = yield request().set('test', 'foobar').get(path);
                const res2 = yield request().set('test', 'barfoo').get(path);
                assert.notEqual(res1.body, res2.body);
            }));
            it('replays outbound headers', () => __awaiter(void 0, void 0, void 0, function* () {
                // Setup routes for this test
                const path = '/header';
                const route = service.route({ path: path, method: 'GET' }, (request) => __awaiter(void 0, void 0, void 0, function* () {
                    return new microservice_1.HttpMessageBuilder()
                        .setHeader('test', request.path.split('test=')[1]).build();
                }));
                const val = String(Date.now());
                const query = 'test=' + val;
                const res1 = yield request().get(`${path}?${query}`);
                const cache = yield service.db.get(`
                    SELECT * FROM cache
                    ORDER BY timestamp DESC LIMIT 1`);
                const res2 = yield request().get(`${path}?${query}`);
                const stored = JSON.parse(cache.headers_out);
                assert.equal(res1.headers.test, val);
                assert.equal(res1.headers.test, stored.test);
                assert.equal(res1.headers.test, res2.headers.test);
                // Clear routes created by this test
                service.unroute(route);
            }));
        });
        describe(`${name} route`, () => {
            it('route() and unroute()', () => __awaiter(void 0, void 0, void 0, function* () {
                const path = '/testroute';
                const opts = service.route(path, _ => 'OK');
                const res1 = yield request().get(path);
                assert.equal(res1.statusCode, 200);
                const flag = service.unroute(opts);
                assert.equal(flag, true);
                const res2 = yield request().get(path);
                assert.equal(res2.statusCode, 404);
            }));
            it('route() return types', () => __awaiter(void 0, void 0, void 0, function* () {
                let idx = 0;
                const path_ = () => `/messagetype${++idx}`;
                // Proper return type
                let path = path_();
                let opts = service.route(path, _ => new microservice_1.HttpMessageBuilder().setBody('OK').build());
                let res = yield request().get(path);
                assert.equal(res.statusCode, 200);
                service.unroute(opts);
                // Plain string
                path = path_();
                opts = service.route(path, _ => 'OK');
                res = yield request().get(path);
                assert.equal(res.statusCode, 200);
                assert.equal(res.body, 'OK');
                service.unroute(opts);
                // Empty string
                path = path_();
                opts = service.route(path, _ => '');
                res = yield request().get(path);
                assert.equal(res.statusCode, 200);
                assert.equal(res.body, '');
                service.unroute(opts);
                // Object type
                path = path_();
                const obj = { data: 'OK' };
                opts = service.route(path, _ => obj);
                res = yield request().get(path);
                assert.equal(res.statusCode, 200);
                assert.equal(res.body, JSON.stringify(obj));
                assert.deepEqual(res.json, obj);
                service.unroute(opts);
            }));
            it('with truthy mandatory parameters', () => __awaiter(void 0, void 0, void 0, function* () {
                const res1 = yield request().get('/params');
                const res2 = yield request().get('/params?test=null');
                assert.equal(400, res1.statusCode);
                assert.equal(200, res2.statusCode);
            }));
            it('with falsy mandatory parameters', () => __awaiter(void 0, void 0, void 0, function* () {
                const res1 = yield request().get('/params');
                const res2 = yield request().get('/params?test=');
                assert.equal(400, res1.statusCode);
                assert.equal(200, res2.statusCode);
            }));
            it('callback `conn` parameter', () => __awaiter(void 0, void 0, void 0, function* () {
                const path = `/${Date.now()}`;
                const checkConn = (conn) => name === 'WebSocketRequest' ? !!conn : !conn;
                const opts = service.route(path, (request, params, conn) => new microservice_1.HttpMessageBuilder().setStatusCode(checkConn(conn) ? 200 : 400).build());
                const res = yield request().get(path);
                assert.equal(res.statusCode, 200);
                service.unroute(opts);
            }));
            it('POST request', () => __awaiter(void 0, void 0, void 0, function* () {
                const path = '/post';
                const opts = service.route({ path: path, method: 'POST' }, (request) => __awaiter(void 0, void 0, void 0, function* () { return (yield request.body) || ''; }));
                const res1 = yield request().get(path);
                const res2 = yield request().post(path);
                const res3 = yield request().post(path, '{}');
                const res4 = yield request().post(path, 'OK');
                assert.equal(res1.statusCode, 404);
                assert.equal(res2.statusCode, 200);
                assert.equal(res3.statusCode, 200);
                assert.equal(res4.statusCode, 200);
                assert.equal(res2.body, '');
                assert.equal(res3.body, '{}');
                assert.equal(res4.body, 'OK');
                service.unroute(opts);
            }));
            // TODO: test DELETE, POST, PUT routes
        });
        describe(`${name} benchmark`, () => {
            // Disable logging during benchmark
            let tmplog = null;
            beforeEach(() => __awaiter(void 0, void 0, void 0, function* () {
                tmplog = service.log;
                service.log = (level, ...args) => __awaiter(void 0, void 0, void 0, function* () { return null; });
            }));
            afterEach(() => {
                service.log = tmplog;
            });
            const _timeLoopDefaults = {
                targetPerSecond: 50,
                iterationCount: 1000,
                warmupCount: 250,
                batchSize: 8
            };
            function timeLoop(func, opts = {}) {
                return __awaiter(this, void 0, void 0, function* () {
                    opts = Object.assign({}, opts, _timeLoopDefaults);
                    // Do not time the first `skip` iterations of the loop
                    for (let i = 0; i < opts.warmupCount; i++) {
                        yield func();
                    }
                    // Start the clock and run the remaining iterations
                    const start = Date.now();
                    const awaitPool = [];
                    for (let i = 0; i < opts.iterationCount; i++) {
                        awaitPool.push(func());
                        if (awaitPool.length >= opts.batchSize) {
                            yield Promise.all(awaitPool);
                            awaitPool.splice(0, awaitPool.length);
                        }
                    }
                    yield Promise.all(awaitPool);
                    // Compute the iterations per seconds and assert that we meet target
                    const totaltime = Date.now() - start;
                    const persecond = (opts.iterationCount - opts.warmupCount) / (totaltime / 1000.0);
                    const msg = `Requests per second: ${persecond.toFixed(2)}. Total time: ${totaltime} ms`;
                    console.log(msg);
                    assert(persecond > opts.targetPerSecond, msg);
                });
            }
            it('random route', () => __awaiter(void 0, void 0, void 0, function* () {
                const req = request();
                yield timeLoop(() => req.get(`/${Date.now()}`));
            })).timeout(20000);
            it('database read', () => __awaiter(void 0, void 0, void 0, function* () {
                const req = request();
                const path = '/dbreadtest';
                const opts = service.route({ path: path, method: 'POST' }, (_) => __awaiter(void 0, void 0, void 0, function* () { return yield service.db.get(`SELECT * FROM logs`); }));
                yield timeLoop(() => req.get(`/dbreadtest?ts=${Date.now()}`));
                service.unroute(opts);
            })).timeout(20000);
            it('large body', () => __awaiter(void 0, void 0, void 0, function* () {
                // Create large text
                let text = '';
                for (let i = 0; i < 1024; i++)
                    text += Math.random().toString(36).substr(2);
                // for (let i = 0; i < 16; i++)
                //     text += text
                const req = request();
                const path = '/largebodytest';
                const opts = service.route({ path: path, method: 'POST' }, (_) => __awaiter(void 0, void 0, void 0, function* () { return text; }));
                // Start test loop and destroy routes after done
                yield timeLoop(() => __awaiter(void 0, void 0, void 0, function* () { return req.get(`/largebodytest?ts=${Date.now()}`); }));
                service.unroute(opts);
            })).timeout(20000);
            it('cached', () => __awaiter(void 0, void 0, void 0, function* () {
                const req = request();
                const path = '/cached';
                const opts = service.route({ path: path, method: 'POST' }, (_) => __awaiter(void 0, void 0, void 0, function* () { return 'OK'; }));
                yield timeLoop(() => req.get(path));
                service.unroute(opts);
            })).timeout(20000);
            it('request flooding', () => __awaiter(void 0, void 0, void 0, function* () {
                const req = request();
                yield timeLoop(() => req.get(`/${Date.now()}`), { batchSize: 1000 });
            })).timeout(20000);
        });
    });
});
