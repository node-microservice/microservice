"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const assert = require("assert");
// import * as request from 'supertest'
const request_1 = require("../request");
const microservice_1 = require("../microservice");
describe('manager', () => {
    let service;
    before(() => __awaiter(void 0, void 0, void 0, function* () {
        service = yield microservice_1.MicroService.create();
        service.route('/', (_) => __awaiter(void 0, void 0, void 0, function* () { return new microservice_1.HttpMessageBuilder().setBody('Hello World').build(); }));
        service.route('/object', (_) => __awaiter(void 0, void 0, void 0, function* () { return new microservice_1.HttpMessageBuilder().setBody({ 0: Math.random() }).build(); }));
        service.route('/random', (_) => __awaiter(void 0, void 0, void 0, function* () { return new microservice_1.HttpMessageBuilder().setBody('' + Math.random()).build(); }));
        service.route({ path: '/params', method: 'GET', mandatoryQueryParameters: ['test'] }, (_) => __awaiter(void 0, void 0, void 0, function* () { return new microservice_1.HttpMessageBuilder().setBody('' + Math.random()).build(); }));
    }));
    beforeEach(() => __awaiter(void 0, void 0, void 0, function* () {
        service.stop();
        yield service.clearCache();
        service.start(Math.floor(Math.random() * (9999 - 1024)) + 1024);
    }));
    afterEach(() => {
        service.stop();
    });
    describe(`Connection`, () => {
        it('Microservice talks back', () => __awaiter(void 0, void 0, void 0, function* () {
            const service = yield microservice_1.MicroService.create();
            service.start(Math.floor(Math.random() * (9999 - 1024)) + 1024);
            const path = '/testroute';
            const opts = service.route(path, (request, params, conn) => {
                conn.send(new microservice_1.HttpMessageBuilder().setBody('1').build());
                conn.send(new microservice_1.HttpMessageBuilder().setBody('2').build());
                return new microservice_1.HttpMessageBuilder().setBody('OK').build();
            });
            const responses = [];
            const request = new request_1.WebSocketRequest(service.server);
            const manager = yield request.manager;
            manager.on('message', message => responses.push(message.body.toString('utf-8')));
            const res = yield request.get(path);
            assert.equal(res.statusCode, 200);
            assert.equal(res.body.toString('utf-8'), 'OK');
            assert.deepEqual(responses, ['1', '2', 'OK']);
            service.stop();
        }));
    });
});
