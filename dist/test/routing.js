"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const assert = require("assert");
const httpmessage_1 = require("../httpmessage");
const routing_1 = require("../routing");
describe('router', () => {
    describe('matches', () => {
        function testRoute(routeOptions, callback, request, body, code = 200) {
            return __awaiter(this, void 0, void 0, function* () {
                // Setup router
                const router = new routing_1.Router();
                const opts = router.set(routeOptions, callback);
                // Attempt match
                const res1 = yield router.match(request);
                assert.equal(res1.statusCode, code);
                assert.equal(res1.body, body);
                // Remote route and attempt match again
                router.remove(opts);
                const res2 = yield router.match(request);
                assert.equal(res2.statusCode, 404);
            });
        }
        ['DELETE', 'GET', 'PATCH', 'POST', 'PUT'].forEach((method) => {
            it(`${method} exact path`, () => __awaiter(void 0, void 0, void 0, function* () {
                const rnd = String(Date.now());
                const opts = { path: `/${rnd}`, method: method };
                const callback = () => rnd;
                const request = new httpmessage_1.HttpMessageBuilder()
                    .setMethod(method)
                    .setPath(`/${rnd}`);
                yield testRoute(opts, callback, request, rnd);
            }));
            it(`${method} root path`, () => __awaiter(void 0, void 0, void 0, function* () {
                const rnd = String(Date.now());
                const opts = { path: `/`, method: method };
                const callback = () => rnd;
                const request = new httpmessage_1.HttpMessageBuilder()
                    .setMethod(method)
                    .setPath(`/`);
                yield testRoute(opts, callback, request, rnd);
            }));
            it(`${method} simple path with one parameter`, () => __awaiter(void 0, void 0, void 0, function* () {
                const paramName = 'param1';
                const rnd = String(Date.now());
                const opts = { path: `/:${paramName}`, method: method };
                const callback = (_, params) => params[paramName];
                const request = new httpmessage_1.HttpMessageBuilder()
                    .setMethod(method)
                    .setPath(`/${rnd}`);
                yield testRoute(opts, callback, request, rnd);
            }));
            it(`${method} leading path with one parameter`, () => __awaiter(void 0, void 0, void 0, function* () {
                const paramName = 'param1';
                const rnd = String(Date.now());
                const opts = { path: `/a/b/c/:${paramName}`, method: method };
                const callback = (_, params) => params[paramName];
                const request = new httpmessage_1.HttpMessageBuilder()
                    .setMethod(method)
                    .setPath(`/a/b/c/${rnd}`);
                yield testRoute(opts, callback, request, rnd);
            }));
            it(`${method} trailing path with one parameter`, () => __awaiter(void 0, void 0, void 0, function* () {
                const paramName = 'param1';
                const rnd = String(Date.now());
                const opts = { path: `/:${paramName}/a/b/c`, method: method };
                const callback = (_, params) => params[paramName];
                const request = new httpmessage_1.HttpMessageBuilder()
                    .setMethod(method)
                    .setPath(`/${rnd}/a/b/c`);
                yield testRoute(opts, callback, request, rnd);
            }));
            it(`${method} trailing optional path`, () => __awaiter(void 0, void 0, void 0, function* () {
                const paramName = 'param1';
                const rnd = String(Date.now());
                const opts = { path: `/a/b?`, method: method };
                const callback = () => rnd;
                const request1 = new httpmessage_1.HttpMessageBuilder()
                    .setMethod(method)
                    .setPath(`/a`);
                const request2 = new httpmessage_1.HttpMessageBuilder()
                    .setMethod(method)
                    .setPath(`/a/b`);
                yield testRoute(opts, callback, request1, rnd);
                yield testRoute(opts, callback, request2, rnd);
            }));
            it(`${method} trailing optional path with one parameter`, () => __awaiter(void 0, void 0, void 0, function* () {
                const paramName = 'param1';
                const rnd = String(Date.now());
                const opts = { path: `/:${paramName}/a?`, method: method };
                const callback = (_, params) => params[paramName];
                const request1 = new httpmessage_1.HttpMessageBuilder()
                    .setMethod(method)
                    .setPath(`/${rnd}`);
                const request2 = new httpmessage_1.HttpMessageBuilder()
                    .setMethod(method)
                    .setPath(`/${rnd}/a`);
                yield testRoute(opts, callback, request1, rnd);
                yield testRoute(opts, callback, request2, rnd);
            }));
            it(`${method} trailing optional parameter`, () => __awaiter(void 0, void 0, void 0, function* () {
                const paramName = 'param1';
                const rnd = String(Date.now());
                const opts = { path: `/a/:${paramName}?`, method: method };
                const callback = (_, params) => params[paramName];
                const request1 = new httpmessage_1.HttpMessageBuilder()
                    .setMethod(method)
                    .setPath(`/a`);
                const request2 = new httpmessage_1.HttpMessageBuilder()
                    .setMethod(method)
                    .setPath(`/a/${rnd}`);
                yield testRoute(opts, callback, request1, '');
                yield testRoute(opts, callback, request2, rnd);
            }));
            it(`${method} parameter followed by optional parameter`, () => __awaiter(void 0, void 0, void 0, function* () {
                const paramName1 = 'param1';
                const paramName2 = 'param2';
                const rnd1 = String(Date.now());
                const rnd2 = String(Math.random()).substr(2);
                const opts = {
                    path: `/:${paramName1}/:${paramName2}?`, method: method
                };
                const callback = (_, params) => `${params[paramName1] || ''}:${params[paramName2] || ''}`;
                const request1 = new httpmessage_1.HttpMessageBuilder()
                    .setMethod(method)
                    .setPath(`/${rnd1}`);
                const request2 = new httpmessage_1.HttpMessageBuilder()
                    .setMethod(method)
                    .setPath(`/${rnd1}/${rnd2}`);
                yield testRoute(opts, callback, request1, `${rnd1}:`);
                yield testRoute(opts, callback, request2, `${rnd1}:${rnd2}`);
            }));
            it(`${method} path without starting slash`, () => __awaiter(void 0, void 0, void 0, function* () {
                const rnd = String(Date.now());
                const opts = { path: `${rnd}`, method: method };
                const callback = () => rnd;
                const request = new httpmessage_1.HttpMessageBuilder()
                    .setMethod(method)
                    .setPath(`/${rnd}`);
                yield testRoute(opts, callback, request, rnd);
            }));
            it(`${method} path with trailing slash`, () => __awaiter(void 0, void 0, void 0, function* () {
                const rnd = String(Date.now());
                const opts1 = { path: `/${rnd}`, method: method };
                const opts2 = { path: `/${rnd}/`, method: method };
                const callback = () => rnd;
                const request1 = new httpmessage_1.HttpMessageBuilder()
                    .setMethod(method)
                    .setPath(`/${rnd}`);
                const request2 = new httpmessage_1.HttpMessageBuilder()
                    .setMethod(method)
                    .setPath(`/${rnd}/`);
                yield testRoute(opts1, callback, request1, rnd);
                yield testRoute(opts1, callback, request2, '', 404);
                yield testRoute(opts2, callback, request2, rnd);
                yield testRoute(opts2, callback, request1, '', 404);
            }));
            it(`${method} path with two slashes`, () => __awaiter(void 0, void 0, void 0, function* () {
                const rnd = String(Date.now());
                const opts = { path: `/a//b`, method: method };
                const callback = () => rnd;
                const request = new httpmessage_1.HttpMessageBuilder()
                    .setMethod(method)
                    .setPath(`/a//b`);
                yield testRoute(opts, callback, request, rnd);
            }));
            it(`${method} path with empty trailing parameter`, () => __awaiter(void 0, void 0, void 0, function* () {
                const paramName = 'param1';
                const opts = { path: `/a/:${paramName}`, method: method };
                const callback = (_, params) => params[paramName];
                const request1 = new httpmessage_1.HttpMessageBuilder()
                    .setMethod(method)
                    .setPath(`/a`);
                const request2 = new httpmessage_1.HttpMessageBuilder()
                    .setMethod(method)
                    .setPath(`/a/`);
                yield testRoute(opts, callback, request1, '', 404);
                yield testRoute(opts, callback, request2, '', 200);
            }));
            it(`${method} path with empty leading parameter`, () => __awaiter(void 0, void 0, void 0, function* () {
                const paramName = 'param1';
                const opts = { path: `/:${paramName}/a`, method: method };
                const callback = (_, params) => params[paramName];
                const request = new httpmessage_1.HttpMessageBuilder()
                    .setMethod(method)
                    .setPath(`//a`);
                yield testRoute(opts, callback, request, '');
            }));
            it(`${method} path with empty middle parameter`, () => __awaiter(void 0, void 0, void 0, function* () {
                const paramName = 'param1';
                const opts = { path: `/a/:${paramName}/b`, method: method };
                const callback = (_, params) => params[paramName];
                const request = new httpmessage_1.HttpMessageBuilder()
                    .setMethod(method)
                    .setPath(`/a//b`);
                yield testRoute(opts, callback, request, '');
            }));
            it(`${method} async callback`, () => __awaiter(void 0, void 0, void 0, function* () {
                const rnd = String(Date.now());
                const opts = { path: `/${rnd}`, method: method };
                const callback = () => __awaiter(void 0, void 0, void 0, function* () { return rnd; });
                const request = new httpmessage_1.HttpMessageBuilder()
                    .setMethod(method)
                    .setPath(`/${rnd}`);
                yield testRoute(opts, callback, request, rnd);
            }));
            it(`${method} async callback with one parameter`, () => __awaiter(void 0, void 0, void 0, function* () {
                const paramName = 'param1';
                const rnd = String(Date.now());
                const opts = { path: `/:${paramName}`, method: method };
                const callback = (_, params) => __awaiter(void 0, void 0, void 0, function* () { return params[paramName]; });
                const request = new httpmessage_1.HttpMessageBuilder()
                    .setMethod(method)
                    .setPath(`/${rnd}`);
                yield testRoute(opts, callback, request, rnd);
            }));
            it(`${method} with query string`, () => __awaiter(void 0, void 0, void 0, function* () {
                const rnd1 = String(Date.now());
                const rnd2 = String(Math.random()).substr(2);
                const opts = { path: `/${rnd1}`, method: method };
                const callback = () => rnd2;
                const request = new httpmessage_1.HttpMessageBuilder()
                    .setMethod(method)
                    .setPath(`/${rnd1}?ts=${rnd2}`);
                yield testRoute(opts, callback, request, rnd2);
            }));
            it(`${method} parameter and query string`, () => __awaiter(void 0, void 0, void 0, function* () {
                const paramName = 'param1';
                const rnd1 = String(Date.now());
                const rnd2 = String(Math.random()).substr(2);
                const opts = { path: `/:${paramName}`, method: method };
                const callback = (_, params) => params[paramName];
                const request = new httpmessage_1.HttpMessageBuilder()
                    .setMethod(method)
                    .setPath(`/${rnd1}?q=${rnd2}`);
                yield testRoute(opts, callback, request, rnd1);
            }));
            it(`${method} throw error`, () => __awaiter(void 0, void 0, void 0, function* () {
                const rnd = String(Date.now());
                const opts = { path: `/${rnd}`, method: method };
                const error = new Error('Intentional error');
                const callback = () => { throw (error); };
                const request = new httpmessage_1.HttpMessageBuilder()
                    .setMethod(method)
                    .setPath(`/${rnd}`);
                yield testRoute(opts, callback, request, 'Server error: ' + error.message, 500);
            }));
        });
    });
});
