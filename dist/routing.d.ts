/// <reference types="node" />
import * as WebSocket from 'ws';
import { IncomingMessage, ServerResponse } from 'http';
import { WebSocketManager } from './wsmanager';
import { AwaitableHttpMessage, HttpMethod, HttpMessage } from './httpmessage';
export declare type RouteParameters = {
    [key: string]: string;
};
export declare type RouteCallback = (message: HttpMessage, params?: RouteParameters, conn?: WebSocketManager) => AwaitableHttpMessage;
export interface MatchOptions {
    expr: RegExp;
    depth: number;
    params: string[];
    method: HttpMethod;
}
export interface RouteOptions {
    path: string;
    method?: HttpMethod;
    optionalPostParameters?: string[];
    mandatoryPostParameters?: string[];
    optionalQueryParameters?: string[];
    mandatoryQueryParameters?: string[];
}
export declare const _defaultRouteOptions: RouteOptions;
/**
 * Route matching and handling for HTTP connections
 */
export declare class Router {
    private invalidParameterName;
    private routes;
    private options;
    constructor();
    private parse;
    set(options: RouteOptions, callback: RouteCallback): MatchOptions;
    remove(options: MatchOptions): boolean;
    match(message: HttpMessage, conn?: WebSocketManager): Promise<HttpMessage>;
    httpRequestHandlerFactory(): (request: IncomingMessage, response: ServerResponse) => Promise<void>;
    addWebSocketListener(manager: WebSocketManager): void;
    webSocketHandler(): (socket: WebSocket) => Promise<void>;
}
