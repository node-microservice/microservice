"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const moment = require("moment");
const sqlite = require("sqlite");
const ootils_1 = require("ootils");
const timers_1 = require("timers");
const http_1 = require("http");
const ws_1 = require("ws");
const routing_1 = require("./routing");
const httpmessage_1 = require("./httpmessage");
// Re-exports
var routing_2 = require("./routing");
exports.Router = routing_2.Router;
var request_1 = require("./request");
exports.HttpRequest = request_1.HttpRequest;
exports.WebSocketRequest = request_1.WebSocketRequest;
var wsmanager_1 = require("./wsmanager");
exports.WebSocketManager = wsmanager_1.WebSocketManager;
var httpmessage_2 = require("./httpmessage");
exports.HttpMessage = httpmessage_2.HttpMessage;
exports.HttpMessageBuilder = httpmessage_2.HttpMessageBuilder;
exports.URLFormatter = httpmessage_2.URLFormatter;
/**
 * Represents a self-contained service with its own routes, cache and backing storage
 */
class MicroService {
    // Defeat instantiation
    constructor() { }
    static create(dbfilename, cacheExpirySeconds = 60, logExpiryDays = 30) {
        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            const service = new MicroService();
            // Database setup
            service.logExpiryDays = logExpiryDays;
            service.cacheExpirySeconds = cacheExpirySeconds;
            service.db = yield sqlite.open(dbfilename || ':memory:');
            yield service.db.run(`
                CREATE TABLE IF NOT EXISTS log (
                    level TEXT NOT NULL,
                    message TEXT NOT NULL,
                    timestamp DATETIME DEFAULT (STRFTIME('%Y-%m-%d %H:%M:%f', 'NOW')))`);
            yield service.db.run(`PRAGMA journal_mode = WAL;`);
            yield service.db.run(`PRAGMA busy_timeout = 15000;`);
            // Initialize cache
            yield service.db.run(`
                CREATE TABLE IF NOT EXISTS cache (
                    url           TEXT NOT NULL,
                    headers_in    TEXT NOT NULL,
                    headers_out   TEXT NOT NULL,
                    body_in       TEXT NOT NULL,
                    body_out      TEXT NOT NULL,
                    is_json       INT,
                    status        INT,
                    timestamp     DATETIME DEFAULT (STRFTIME('%Y-%m-%d %H:%M:%f', 'NOW')))`);
            yield service.clearCache();
            // Routing setup
            service.router = new routing_1.Router();
            // Resolve with service now that database has been initialized
            resolve(service);
        }));
    }
    /** Logs an event in the console as well as the internal database. Level can be E, W, I and V */
    log(level, ...args) {
        return __awaiter(this, void 0, void 0, function* () {
            if (level.length > 1) {
                args = [level].concat(args);
                level = 'I';
            }
            const fmt = 'YYYY-MM-DDTHH:mm:ss.SSS';
            console.log.apply(this, [moment().utc().format(fmt), level].concat(args));
            try {
                yield this.db.run(`INSERT INTO log (level, message) VALUES (?, ?)`, [level, args.join('\t')]);
            }
            catch (err) {
                console.log.apply(this, [moment().utc().format(fmt), 'E', 'Error updating log: ' + err.message]);
            }
            return args.join(' ');
        });
    }
    clearLogs(all = false) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const prev = `DATETIME('NOW', '-${this.logExpiryDays} days')`;
                const cond = all ? '' : `WHERE timestamp < (STRFTIME('%Y-%m-%d %H:%M:%f', ${prev}))`;
                yield this.db.run(`DELETE FROM log ${cond}`);
            }
            catch (err) {
                this.log('E', err.message);
            }
        });
    }
    clearCache(all = false) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const prev = `DATETIME('NOW', '-${this.cacheExpirySeconds} seconds')`;
                const cond = all ? '' : `WHERE timestamp < (STRFTIME('%Y-%m-%d %H:%M:%f', ${prev}))`;
                yield this.db.run(`DELETE FROM cache ${cond}`);
            }
            catch (err) {
                this.log('E', err.message);
            }
        });
    }
    queryCache(request) {
        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            try {
                const prev = `DATETIME('NOW', '-${this.cacheExpirySeconds} seconds')`;
                const row = yield this.db.get(`
                    SELECT * FROM cache WHERE url=(?) AND body_in=(?) AND headers_in=(?)
                    AND timestamp > (STRFTIME('%Y-%m-%d %H:%M:%f', ${prev})) LIMIT 1`, [request.path, JSON.stringify(request.body), JSON.stringify(request.headers)]);
                if (row && row.status !== 503) {
                    this.log('V', `[${row.status}] Serving result from cache`);
                    const body = Buffer.from(JSON.parse(row.body_out));
                    resolve(new httpmessage_1.HttpMessageBuilder()
                        .setPath(row.url)
                        .setHeaders(JSON.parse(row.headers_out))
                        .setStatusCode(row.status)
                        .setBody(row.is_json ? JSON.parse(body.toString('utf-8')) : body));
                }
                else {
                    resolve();
                }
            }
            catch (err) {
                this.log('E', err.message);
                resolve();
            }
        }));
    }
    updateCache(request, response) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this.db.run(`
                INSERT INTO cache (url, status, body_in, body_out, is_json, headers_in, headers_out)
                VALUES (?, ?, ?, ?, ?, ?, ?)`, [
                    request.path, response.statusCode,
                    JSON.stringify(request.body), JSON.stringify(response.body), response.json,
                    JSON.stringify(request.headers), JSON.stringify(response.headers)
                ]);
            }
            catch (err) {
                this.log('E', 'Error occurred updating cache: ' + err.message);
            }
        });
    }
    wrapCallback(options, callback) {
        // Log event and return wrapped function
        return (request, params, conn) => __awaiter(this, void 0, void 0, function* () {
            this.log('V', options.method, request.path);
            // Early exit: return from cache if available
            const data = yield this.queryCache(request);
            if (data)
                return data;
            // Perform actual callback
            const response = yield httpmessage_1.HttpMessage.parse(callback(request, params, conn));
            this.updateCache(request, response); // intentionally not awaited
            return response;
        });
    }
    /**
     * Sets up a route for the MicroService to handle
     * @param options either a `RouteOptions` or a string with the path being registered
     * @param callback the callback function that will be performed by this route for every request
     */
    route(options, callback) {
        if (typeof options === 'string')
            options = { path: options };
        const options_ = Object.assign({}, routing_1._defaultRouteOptions, options);
        this.log('V', `Setting up ${options_.method} route for ${options_.path}`);
        // Wrap the callback to allow caching
        return this.router.set(options_, this.wrapCallback(options_, callback));
    }
    /**
     * Removes the specified route
     * @param options the MatchOptions returned from the call to `route()`
     */
    unroute(options) {
        return this.router.remove(options);
    }
    /**
     * Signal other MicroService-aware components, such as SystemD services and
     * microservice-spawner that the service is ready.
     */
    notify() {
        return new Promise((resolve, reject) => {
            // Signal parent process that server has started by disconnecting from the IPC channel
            if (process.connected) {
                process.disconnect();
            }
            // Signal systemd service (ignore if systemd-notify does not exist)
            ootils_1.ProcShell.which('systemd-notify').then(bin => {
                ootils_1.ProcShell.exec(`${bin} --ready`).catch(err => {
                    this.log('E', 'Unable to notify systemd daemon: ' + err.message);
                    reject(err);
                }).then(resolve);
            }).catch(err => resolve());
        });
    }
    start(port) {
        this.log('I', 'Starting microservice at port ' + port);
        // Open the HTTP server
        this.server = new http_1.Server(this.router.httpRequestHandlerFactory());
        this.server.listen(port);
        // Open the web socket server
        this.wss = new ws_1.Server({ server: this.server });
        this.wss.on('connection', this.router.webSocketHandler());
        // Setup error handlers
        this.server.on('error', err => this.log('E', err.message));
        this.wss.on('error', err => this.log('E', err.message));
        // Setup maintenance window to scrub logs and cache on a timely basis
        this.maintenanceTimer = timers_1.setInterval(() => __awaiter(this, void 0, void 0, function* () {
            yield this.clearLogs();
            yield this.clearCache();
        }), 1 * 60 * 1000); // Every minute
    }
    stop() {
        clearInterval(this.maintenanceTimer);
        if (this.server && this.server.listening) {
            this.server.removeAllListeners();
            this.server.close();
            this.server = null;
        }
        if (this.wss) {
            this.wss.removeAllListeners();
            this.wss.close();
            this.wss = null;
        }
    }
    port() {
        return this.server.listening && this.server.address().port;
    }
}
exports.MicroService = MicroService;
