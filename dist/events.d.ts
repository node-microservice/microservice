/**
 * From https://github.com/kimamula/TypeScript-definition-of-EventEmitter-with-keyof
 */
export interface ITypedEventEmitter<T> {
    listeners<K extends keyof T>(event: K): ((arg: T[K]) => any)[];
    addListener<K extends keyof T>(event: K, listener: (arg: T[K]) => any): this;
    on<K extends keyof T>(event: K, listener: (arg: T[K]) => any): this;
    once<K extends keyof T>(event: K, listener: (arg: T[K]) => any): this;
    removeListener<K extends keyof T>(event: K, listener: (arg: T[K]) => any): this;
    removeAllListeners<K extends keyof T>(event?: K): this;
    emit<K extends keyof T>(event: K, arg: T[K]): boolean;
    listenerCount<K extends keyof T>(event: K): number;
    prependListener<K extends keyof T>(event: K, listener: (arg: T[K]) => any): this;
}
/**
 * Generic implementation of ITypedEventEmitter, to be subclassed for actual use
 */
export declare class TypedEventEmitter<T> implements ITypedEventEmitter<T> {
    private _listeners;
    constructor();
    listeners<K extends keyof T>(event: K): ((arg: T[keyof T]) => any)[];
    addListener<K extends keyof T>(event: K, listener: (arg: T[K]) => any): this;
    prependListener<K extends keyof T>(event: K, listener: (arg: T[K]) => any): this;
    removeListener<K extends keyof T>(event: K, listener: (arg: T[K]) => any): this;
    removeAllListeners<K extends keyof T>(event?: K): this;
    on<K extends keyof T>(event: K, listener: (arg: T[K]) => any): this;
    once<K extends keyof T>(event: K, listener: (arg: T[K]) => any): this;
    emit<K extends keyof T>(event: K, arg: T[K]): boolean;
    listenerCount<K extends keyof T>(event: K): number;
}
