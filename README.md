# MicroService

Core class representing a single service. Enables users to instantiate a new Node + Express application with built-in caching, SystemD bindings and support for [`orchestrator`](https://gitlab.com/node-microservice/orchestrator).

## API Documentation

TODO

## Benchmarks

The following are results from benchmark testing on a Macbook Pro 2015:

| Test            | Requests per second |
| --------------- | ------------------- |
| Cached requests | 1141.55             |
| `Math.random()` | 879.51              |
| 4xx status      | 696.86              |

To run your own benchmarks, simply run: `npm run benchmark`.

## Related Projects

This project is only the most basic building block for a microservice architecture, and it is insufficient for moderately complex projects. Instead of building off this project, it is recommended to start by forking the [template](https://gitlab.com/node-microservice/template). At minimum, a medium-sized project will also require the use of the [orchestrator](https://gitlab.com/node-microservice/orchestrator). Before building a new microservice, take a look at the following somewhat-generic pre-built microservices to ensure none of them already fit your needs:

- [Static](https://gitlab.com/node-microservice/static): Serves static files from a directory.
- [Spawner](https://gitlab.com/node-microservice/spawner): Spawns other microservices given their Gitlab or Github location.